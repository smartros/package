﻿9:13 오전 월요일, 6월 22,2015
android\
- mobilephone node 와 연동하여 사용할 시, marker 인식 오류 수정
- 얼굴인식 기능 추가

mobilephone\
- android node와 연동시 camera 충동 오류 수정
parrot\
- parrot emergency, forward, backward , battery 기능 추가 구현 

3:28 오후 금요일, 6월 19, 2015
altino\
- 전방 조향 모터 5단계 조절 추가
- 후진 모터 속도 입력 추가

android\
- 마커 인식 기능 추가

10:17 오전 목요일, 6월 18, 2015

altino\  
-
smartros-core 변경(node name 추가)  
player에서 start/stop 이벤트 발생 시에 데이터 전송 및 중지  
android\  
-
smartros-core 변경(node name 추가)  
speech recognition 수정, protocol 범위 체크 
arduino\
-
smartros-core 변경(node name 추가)  
clebo\
-
smartros-core 변경(node name 추가)  
empty\
-
smartros-core 변경(node name 추가)  
makeblock\
-
smartros-core 변경(node name 추가)  
mobilephone\
-
smartros-core 변경(node name 추가)  
modulo5\
-
smartros-core 변경(node name 추가)  
parrot\
-
smartros-core 변경(node name 추가)  

----------


10:58 오전 수요일, 6월 10, 2015

first commit

