(function (ext) {

	ext._shutdown = function() {};
	ext._getStatus = function() {
		return { status:2, msg: 'Ready' };
	};
	var descriptor = {
		blocks: [
				[" ", "send message %s", "em_send_message"],
				["r", "get message", "em_get_message"],
        ],
		menus: {
		},
		url: 'http://cafe.naver.com/smartros',
		port: 50007,
		node: 'com.isans.smartros.empty'
		
	};
	ScratchExtensions.register('Empty', descriptor, ext);
})({});