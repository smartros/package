(function (ext) {

	ext._shutdown = function() {};
	ext._getStatus = function() {
		return { status:2, msg: 'Ready' };
	};
	var descriptor = {
		blocks: [
				[" ", "set pin %n as %m.mode", "pinMode", 2, "Digital Input"],
				[" ", "digital write pin %n %m.highLow", "digitalWrite", 13, "high"],
				[" ", "analog write pin %n value %n", "analogWrite", 3, 255],
				[" ", "servo write pin %n degrees %n", "servoWrite", 5, 180],
				["b", "digital read pin %n", "digitalRead", 2],
				["r", "analog read pin %n", "analogRead", 0],
        ],
		menus: {			
			"mode": ["Digital Input", "Digital Output","Analog Input","Analog Output(PWM)","Servo"],
			"highLow": ["high", "low"],
			
		},
		url: 'http://cafe.naver.com/smartros',
		port: 50007,
		node: 'com.isans.smartros.arduino'
		
	};
	ScratchExtensions.register('Arduino', descriptor, ext);
})({});