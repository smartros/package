package com.isans.smartros.clebo;

import android.os.Bundle;
import android.util.Log;

import org.ros.android.smartRosActivity;

public class MainActivity extends smartRosActivity {

    private int fps = 1;
    private static final String TAG = "MainActivity";
    private FT311UARTInterface clebo;
    private long startTime = 0;

    public byte[] sendBuf_byte = new byte[9];
    public byte[] sendBuf_byte2 = new byte[8];
    public byte[] sendBuf_byte3 = new byte[6];

    public MainActivity() {
        super("Clebo Node");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        clebo = new FT311UARTInterface(this, null);
        moveTaskToBack(true);   // turn background task

    }

    @Override
    protected void onResume() {
        super.onResume();
        clebo.ResumeAccessory();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        clebo.DestroyAccessory(true);
    }


    @Override
    protected void onNewMessage(String s) {
        Log.d(TAG, s);
        long endTime = System.currentTimeMillis();
        long duration;
        String[] parts = s.split("/");
        switch (parts[1]) {
            case "start_all":
                duration = (endTime - startTime) / 1000;
                startTime = System.currentTimeMillis();
                if (duration > 0) {
                    Log.d(TAG, "start_all");
                }
                clebo.ResumeAccessory();
                break;
            case "reset_all":
                clebo.DestroyAccessory(true);
                break;
            case "fps":
                fps = Integer.parseInt(parts[2]);
                break;
            case "forward":
                Log.d(TAG, "forward");
                String param1 = parts[2];
                int cm = Integer.parseInt(param1);
                sendBuf_byte[5] = (byte) 0x00;//direction
                sendBuf_byte[6] = (byte) (cm % 256);
                sendBuf_byte[7] = (byte) (cm / 256);

                clebo.Move(sendBuf_byte);
                break;
            case "backward":
                sendBuf_byte[5] = (byte) 0x02;
                sendBuf_byte[6] = (byte) 0xB4;
                sendBuf_byte[7] = (byte) 0x00;

                clebo.Move(sendBuf_byte);
                break;
            case "turn-left":
                String param2 = parts[2];
                String param3 = parts[3];
                int angle = Integer.parseInt(param2);

                if (param3.equals("on")) {
                    sendBuf_byte[5] = (byte) 0x01;
                    sendBuf_byte[6] = (byte) (angle % 256);
                    sendBuf_byte[7] = (byte) (angle / 256);
                    clebo.Move(sendBuf_byte);
                }
                break;
            case "turn-right":
                String param4 = parts[2];
                String param5 = parts[3];
                int angle2 = Integer.parseInt(param4);

                if (param5.equals("on")) {
                    sendBuf_byte[5] = (byte) 0x03;
                    sendBuf_byte[6] = (byte) (angle2 % 256);
                    sendBuf_byte[7] = (byte) (angle2 / 256);
                    clebo.Move(sendBuf_byte);
                }
                break;
            case "startclean":
                sendBuf_byte2[5] = (byte) 0x00;
                sendBuf_byte2[6] = (byte) 0x00;
                clebo.Clean(sendBuf_byte2);
                break;
            case "stopclean":
                sendBuf_byte3[4] = (byte) 0x31;
                clebo.stop(sendBuf_byte3);
                break;
            case "stop":
                sendBuf_byte3[4] = (byte) 0x05;
                clebo.stop(sendBuf_byte3);
                break;
            case "send":
                Log.d(TAG, "send");
                sendBuf_byte3[4] = (byte) 0x41;
                clebo.GetBatteryStatus(sendBuf_byte3);
                sendRespMessage(String.format("charge %d\n", clebo.getcharge()));
                sendRespMessage(String.format("capacity %d\n", clebo.getcapacity()));
                break;

            default:
                break;
        }
    }

}
