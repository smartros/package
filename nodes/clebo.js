(function (ext) {

	ext._shutdown = function() {};
	ext._getStatus = function() {
		return { status:2, msg: 'Ready' };
	};
	var descriptor = {
		blocks: [
				[" ", "go forward %n cm move ", "forward",1],
				[" ", "go backward move", "backward"],
				[" ", "stop", "stop"],
				[" ", "turn left %n degree %m.onoff", "turn-left","10","on"],
				[" ", "turn right %n degree %m.onoff", "turn-right","10","on"],
				[" ", "startcleaning", "startclean"],
				[" ", "stopcleaning", "stopclean"],
				[" ", "battery", "send"],
				["r", "get battery charge","charge"],						
				["r", "get battery capacity","capacity"],	
			],
	
		menus: {
			"onoff": ["on", "off"],
		},
		url: 'http://www.isans.co.kr',
		port: 50007,
		node: 'com.isans.smartros.clebo'
	};
	ScratchExtensions.register('iClebo', descriptor, ext);
})({});