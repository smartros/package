(function (ext) {

	ext._shutdown = function() {};
	ext._getStatus = function() {
		return { status:2, msg: 'Ready' };
	};
	var descriptor = {
		blocks: [				
				[" ", "turn %m.turn camera led", "camera-led", "off"],
				[" ", "set vibrator %n sec", "vibrator", 1],	
				["r", "get value(lux) of light sensor", "light"],				
				["r", "get value(cm) of proximity sensor", "proximity"],
				["r", "get %m.coordinate m/s^2", "acceleration", "x-axis"],
				["r", "get %m.directional degree", "orientation", "azimuth"],
				["r", "get check shake","shake"],
				["r", "get shake count","count"],
				[" ", "reset shake count", "shake_reset"],	
				[" ", "setting shake threshold %n","shake_threshold",1000]

				
        ],
		menus: {
			"atomic_sensor": 		["proximity"],
			"directional_sensor": 	["acceleration"],
			"coordinate": 			["x-axis", "y-axis", "z-axis"],
			"turn": 				["on", "off"],
			"directional": 			["azimuth", "pitch", "roll"],
			
		},
		url: 'http://cafe.naver.com/smartros',
		port: 50007,
		node: "com.isans.smartros.mobilephone"
		
	};
	ScratchExtensions.register('Mobilephone', descriptor, ext);
})({});