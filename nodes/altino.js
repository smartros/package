(function (ext) {

	ext._shutdown = function() {};
	ext._getStatus = function() {
		return { status:2, msg: 'Ready' };
	};
	var descriptor = {
		blocks: [
				[" ", "turn %m.turn %m.led led", "led", "off", "front"],
				[" ", "turn %m.turn %m.dot dot line ", "dot", "off", "1"],
				[" ", "turn %m.turn %s dot ascii code", "ascii", "off", "0"],
				[" ", "turn %m.turn motor speed %n", "motor", "off", 200],
				[" ", "turn %m.turn back motor speed %n", "backmotor", "off", 200],
				[" ", "turn %m.turn %n.buzzer buzzer", "buzzer", "off", "0"],
				[" ", "go %m.drive %m.speed", "go", "forward", "medium"],				
				[" ", "turn %m.wheel steering wheel", "wheel","left"],
				[" ", "turn %m.mode %m.wheel %m.degree steering wheel", "setturn","auto","left",1],
				[" ", "stop", "stop"],
				["r", "get %m.sensor", "sensor", "light"],				
			],
	
		menus: {
			"sensor": ["light", "temperature", "torqueR", "torqueL", "ir1", "ir2", "ir3", "ir4", "ir5", "ir6"],
			"turn": ["on", "off"],
			"dot" : ["1", "2", "3", "4", "5", "6", "7", "8", "all"], 
			"speed": ["slow", "medium", "fast"],
			"led" : ["front","back","all","left-signal","right-signal","signal-all"],
			"drive": ["forward", "backward"],
			"wheel": ["left", "right", "straight"],
			"degree":["1","2","3","4"],
			"mode":["auto","step"],
			
		},
		url: 'http://cafe.naver.com/smartros',
		port: 50007,
		node: 'com.isans.smartros.altino'
	};
	ScratchExtensions.register('Altino', descriptor, ext);
})({});