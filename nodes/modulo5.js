(function (ext) {

	ext._shutdown = function() {};
	ext._getStatus = function() {
		return { status:2, msg: 'Ready' };
	};
	var descriptor = {
		blocks: [
				[" ", "go %m.drive speed %n", "m5_drive", "forward", 0.25],
				[" ", "turn %m.direct speed %n", "m5_turn", "left", 0.25],
				[" ", "stop", "stop"],
				[" ", "move pan %n angle", "m5_pan", 180],
				[" ", "run motor %m.motorPort speed %n", "m5_dcmotor", "1", 0.25],			
				["r", "get psd sensor %m.normalPort", "m5_psd", "1"],
				["r", "get gyro sensor %m.orientation", "m5_gyro", "x"],
        ],
		menus: {			
			"orientation": ["x", "y", "z"],
			"drive":["forward", "backward"],
			"direct":["left", "right"],			
			"normalPort":["1","2","3","4"],
			"motorPort":["1","2"],
			
		},
		url: 'http://cafe.naver.com/smartros',
		port: 50007,
		node: 'com.isans.smartros.modulo5'
		
	};
	ScratchExtensions.register('Modulo5', descriptor, ext);
})({});