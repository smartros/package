package com.isans.smartros.minidarwin;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import org.ros.android.smartRosActivity;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;

public class MainActivity extends smartRosActivity {
    BluetoothSPP bt;
    private String bt_address;
    private Protocol protocol;

    public MainActivity(){
        super("MiniDarwin Node");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        moveTaskToBack(true);   // turn background task

        bt = new BluetoothSPP(this);
        protocol = new Protocol(bt);
        if(!bt.isBluetoothAvailable()) {
            Toast.makeText(getApplicationContext()
                    , "Bluetooth is not available"
                    , Toast.LENGTH_SHORT).show();
            finish();
        }
        bt.setOnDataReceivedListener(new BluetoothSPP.OnDataReceivedListener() {
            public void onDataReceived(byte[] data, String message) {
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });

        bt.setBluetoothConnectionListener(new BluetoothSPP.BluetoothConnectionListener() {
            public void onDeviceConnected(String name, String address) {
                Toast.makeText(getApplicationContext()
                        , "Connected to " + name + "\n" + address
                        , Toast.LENGTH_SHORT).show();
                sendRespMessage(String.format("md_connected %d\n", 1));
            }

            public void onDeviceDisconnected() {
                Toast.makeText(getApplicationContext()
                        , "Connection lost", Toast.LENGTH_SHORT).show();
                sendRespMessage(String.format("md_connected %d\n", 0));
            }

            public void onDeviceConnectionFailed() {
                Toast.makeText(getApplicationContext()
                        , "Unable to connect", Toast.LENGTH_SHORT).show();
                sendRespMessage(String.format("md_connected %d\n", 0));
            }
        });
        if(bt.getServiceState() == BluetoothState.STATE_CONNECTED) {
            bt.disconnect();
        }
    }
    @Override
    protected void onNewMessage(String s) {
        String[] parts = s.split("/");
        Log.d("test", s);
        switch(parts[1]) {
            case "md_set_address":
                bt_address = parts[2];
                if(bt.getServiceState() == BluetoothState.STATE_CONNECTED)
                    break;
                bt.connect(bt_address);
                sendRespMessage(String.format("md_connected %d\n", 0));
                break;
            case "md_start_pose":
                protocol.sendData(Protocol.INIT_POSE);
                break;
            case "md_gesture":
                switch(parts[2]){
                    case "sit down":
                        protocol.sendData(Protocol.SIT_DOWN);
                        break;
                    case "stand up":
                        protocol.sendData(Protocol.STAND_UP);
                        break;
                    case "hello":
                        protocol.sendData(Protocol.HELLO);
                        break;
                    case "hello2":
                        protocol.sendData(Protocol.HELLO2);
                        break;
                    case "shaking left hand":
                        protocol.sendData(Protocol.SHAKING_LEFT_HAND);
                        break;
                    case "shaking right hand":
                        protocol.sendData(Protocol.SHAKING_RIGHT_HAND);
                        break;
                }
                break;
            case "md_fighting":
                switch(parts[2]){
                    case "left jab":
                        protocol.sendData(Protocol.LEFT_JAB);
                        break;
                    case "right jab":
                        protocol.sendData(Protocol.RIGHT_JAB);
                        break;
                    case "left hook":
                        protocol.sendData(Protocol.LEFT_HOOK);
                        break;
                    case "right hook":
                        protocol.sendData(Protocol.RIGHT_HOOK);
                        break;
                    case "left uppercut":
                        protocol.sendData(Protocol.LEFT_UPPERCUT);
                        break;
                    case "right uppercut":
                        protocol.sendData(Protocol.RIGHT_UPPERCUT);
                        break;
                    case "left attack":
                        protocol.sendData(Protocol.LEFT_ATTACK);
                        break;
                    case "right attack":
                        protocol.sendData(Protocol.RIGHT_ATTACK);
                        break;
                }
                break;
            case "md_moving":
                switch (parts[2]){
                    case "forward":
                        protocol.sendData(Protocol.FORWARD);
                        break;
                    case "backward":
                        protocol.sendData(Protocol.BACKWARD);
                        break;
                    case "left":
                        protocol.sendData(Protocol.LEFT);
                        break;
                    case "right":
                        protocol.sendData(Protocol.RIGHT);
                        break;
                    case "turn left":
                        protocol.sendData(Protocol.TURN_LEFT);
                        break;
                    case "turn right":
                        protocol.sendData(Protocol.TURN_RIGHT);
                        break;
                }
                break;
            case "md_dance":
                switch (parts[2]){
                    case "b-boy1":
                        protocol.sendData(Protocol.B_BOY1);
                        break;
                    case "b-boy2":
                        protocol.sendData(Protocol.B_BOY2);
                        break;
                    case "gangnam style":
                        protocol.sendData(Protocol.GANGNAM);
                        break;
                }
                break;
        }
    }
    public void onStart() {
        super.onStart();
        if (!bt.isBluetoothEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, BluetoothState.REQUEST_ENABLE_BT);
        } else {
            if(!bt.isServiceAvailable()) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_OTHER);
            }
        }
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if(resultCode == Activity.RESULT_OK)
                bt.connect(data);
        } else if(requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if(resultCode == Activity.RESULT_OK) {
                bt.setupService();
                bt.startService(BluetoothState.DEVICE_OTHER);

            } else {
                Toast.makeText(getApplicationContext()
                        , "Bluetooth was not enabled."
                        , Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
    public void onDestroy() {
        super.onDestroy();
        bt.stopService();
    }
}
