package com.isans.smartros.user;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import org.ros.android.smartRosActivity;

import java.util.Random;

public class MainActivity extends smartRosActivity {

    private String uID;
    private Handler mHandler;
    //private Runnable mRunnable;
    private int fps = 1;
    private boolean bSend = false;
    private static final String TAG = MainActivity.class.getSimpleName();
    private Handler handler;
    public MainActivity(){
        super("User Node");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        moveTaskToBack(true);   // turn background task
/*
        mRunnable = new Runnable() {
            @Override
            public void run() {
                sendRespMessage(String.format("_busy %s\n", uID));
                Log.d("test", "uID: " + uID);
            }
        };
        mHandler = new Handler();
*/
        this.handler = new Handler();
        Thread thread = new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(1000 / fps);
                    handler.post(new Runnable() {
                        public void run() {
                            while (true) {
                                StringBuffer recordValue = new StringBuffer();
                                Random random = new Random();
                                recordValue.append(String.format("sensor/light %d\n", random.nextInt(100)));
                                recordValue.append(String.format("sensor/temperature %d\n", random.nextInt(100)));
                                recordValue.append(String.format("sensor/torqueR %d\n", random.nextInt(100)));
                                recordValue.append(String.format("sensor/torqueL %d\n", random.nextInt(100)));
                                recordValue.append(String.format("sensor/ir1 %d\n", random.nextInt(100)));
                                recordValue.append(String.format("sensor/ir2 %d\n", random.nextInt(100)));
                                recordValue.append(String.format("sensor/ir3 %d\n", random.nextInt(100)));
                                recordValue.append(String.format("sensor/ir4 %d\n", random.nextInt(100)));
                                recordValue.append(String.format("sensor/ir5 %d\n", random.nextInt(100)));
                                recordValue.append(String.format("sensor/ir6 %d\n", random.nextInt(100)));
                                sendRespMessage(recordValue.toString());
                            }
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
    @Override
    protected void onNewMessage(String s) {
        String [] parts = s.split("/");
        Log.d("test", s);
        switch(parts[1]) {
            case "start_all":
                Log.d(TAG, "start_all");
                break;
            case "reset_all":
                Log.d(TAG, "reset_all");
                break;
            case "fps":
                fps = Integer.parseInt(parts[2]);
                break;
            case "us_send_message":
                //sendRespMessage(String.format("us_get_message %s\n", parts[2]));
                //sendRespMessage(String.format("_busy %s\n", parts[2]));
                /*sendRespMessage("_busy \n");
                if( parts.length > 2 ) {
                    uID = parts[2];
                    mHandler.postDelayed(mRunnable, 3000);
                }
                */
                break;
            default:
                break;
        }
    }
}
