package com.isans.smartros.modulo5;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bcc on 2015-06-05.
 */
public class Protocol {
    public static final int		Odometry_Status		=	21;
    public static final int		Target_CoordinateXY		=	22;
    public static final int		Target_CoordinateW		=	23;
    public static final int		Target_Move_DirectionXY	=		24;
    public static final int		Target_Move_DirectionW	=		25;
    public static final int		Target_Move_VW	=		26;
    public static final int		Current_Move_VW	=		27;
    public static final int		Current_CoordinateXY	=		28;
    public static final int		Current_CoordinateW		=	29;
    public static final int		Current_MoveXY		=	30;
    public static final int		Current_MoveW		=	31;
    public static final int		Odometry_Reservation1	=		32;
    public static final int		Odometry_Reservation2	=		33;
    public static final int		Junction_Status		=	34;
    public static final int		Junction_ALL_SUM	=		35;
    public static final int		Junction_CMD		=	36;
    public static final int		Junction_Section_SUM	=		37;
    public static final int		Junction_Reservation1	=		38;
    public static final int		Junction_Reservation2	=		39;
    public static final int		Wheel1_Status	=		40;
    public static final int		W1_Target		=	41;
    public static final int		W1_Current		=	42;
    public static final int		W1_Value_Setting_PI	=		43;
    public static final int		W1_Value_Setting_D	=		44;
    public static final int		W1_Value_Setting_AC	=		45;
    public static final int		Wheel1_Reservation	=		46;
    public static final int		Wheel2_Status = 47;
    public static final int		W2_Target = 48;
    public static final int		W2_Current = 49;
    public static final int		W2_Value_Setting_PI =			50;
    public static final int		W2_Value_Setting_D	=		51;
    public static final int		W2_Value_Setting_AC	=		52;
    public static final int		Wheel2_Reservation	=		53;
    public static final int		Wheel3_Status	=		54;
    public static final int		W3_Target	=		55;
    public static final int		W3_Current	=		56;
    public static final int		W3_Value_Setting_PI	=		57;
    public static final int		W3_Value_Setting_D	=		58;
    public static final int		W3_Value_Setting_AC	=		59;
    public static final int		Wheel3_Reservation	=		60;
    public static final int		Wheel4_Status		=	61;
    public static final int		W4_Target	=		62;
    public static final int		W4_Current	=		63;
    public static final int		W4_Value_Setting_PI	=		64;
    public static final int		W4_Value_Setting_D	=		65;
    public static final int		W4_Value_Setting_AC	=		66;
    public static final int		Wheel4_Reservation	=		67;
    public static final int		Fan_Status	=		68;
    public static final int		F_Target	=		69;
    public static final int		F_Current	=		70;
    public static final int		F_Value_Setting_PI	=		71;
    public static final int		F_Value_Setting_D	=		72;
    public static final int		F_Value_Setting_AC	=		73;
    public static final int		Fan_Reservation		=	74;
    public static final int		Tilt_Status	=		75;
    public static final int		T_Target	=		76;
    public static final int		T_Current	=		77;
    public static final int		T_Value_Setting_PI	=		78;
    public static final int		T_Value_Setting_D	=		79;
    public static final int		T_Value_Setting_AC	=		80;
    public static final int		Tilt_Reservation	=		81;
    public static final int		LED_Status	=		82;
    public static final int		LED_Value1	=		83;
    public static final int		LED_Value2	=		84;
    public static final int		LED_Value3	=		85;
    public static final int		LED_Value4	=		86;
    public static final int		LED_Reservation	=		87;

    public static final int		Gyro_Status	=		164;
    public static final int		Gyro	=		165;
    public static final int		Gyro_Reservation	=		167;
}
