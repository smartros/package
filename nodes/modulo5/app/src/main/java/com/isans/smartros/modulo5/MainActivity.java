package com.isans.smartros.modulo5;

import android.content.Context;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.HexDump;
import com.hoho.android.usbserial.util.SerialInputOutputManager;
import com.isans.smartros.modulo5.R;

import org.ros.android.smartRosActivity;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// ref: https://code.google.com/p/usb-serial-for-android/source/browse/UsbSerialExamples/src
// /com/hoho/android/usbserial/examples/SerialConsoleActivity.java
public class MainActivity extends smartRosActivity {

    private static UsbSerialDriver sDriver = null;
    private UsbManager mUsbManager;
    private byte[] rx = new byte[128];
    private int rxIndex = 0;
    private int fps = 1;
    private Handler handler;
    private static final String TAG = "MainActivity";
    public MainActivity() {
        super("Modulo5 Node");
    }
    private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();

    private SerialInputOutputManager mSerialIoManager;

    private final SerialInputOutputManager.Listener mListener =
        new SerialInputOutputManager.Listener() {
            @Override
            public void onRunError(Exception e) {
                Log.d(TAG, "Runner stopped.");
                sDriver = null;
            }
            @Override
            public void onNewData(final byte[] data) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // parsing code
                    }
                });
            }
        };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        moveTaskToBack(true);   // turn background task
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        sDriver = UsbSerialProber.acquire(mUsbManager);
        this.handler = new Handler();
        Thread thread = new Thread(new Runnable() {
            public void run() {

                while (sDriver != null) {
                    try {
                        Thread.sleep(1000 / fps);
                        handler.post(new Runnable() {
                            public void run() {
                                // periodic writing code
                            }
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "Resumed, sDriver=" + sDriver);
        if (sDriver == null) {
            Log.d(TAG, "No serial device.");
        } else {
            try {
                sDriver.open();
                sDriver.setBaudRate(115200);// , 8, UsbSerialDriver.STOPBITS_1, UsbSerialDriver.PARITY_NONE);
            } catch (IOException e) {
                Log.e(TAG, "Error setting up device: " + e.getMessage(), e);
                try {
                    sDriver.close();
                } catch (IOException e2) {
                    // Ignore.
                }
                sDriver = null;
                return;
            }
            Log.d(TAG, "Serial device: " + sDriver.getClass().getSimpleName());
        }
        onDeviceStateChange();
    }
    private void stopIoManager() {
        if (mSerialIoManager != null) {
            Log.i(TAG, "Stopping io manager ..");
            mSerialIoManager.stop();
            mSerialIoManager = null;
        }
    }

    private void startIoManager() {
        if (sDriver != null) {
            Log.i(TAG, "Starting io manager ..");
            mSerialIoManager = new SerialInputOutputManager(sDriver, mListener);
            mExecutor.submit(mSerialIoManager);
            final String title = String.format("Vendor %s Product %s",
                    HexDump.toHexString((short) sDriver.getDevice().getVendorId()),
                    HexDump.toHexString((short) sDriver.getDevice().getProductId()));
            Toast.makeText(this, "Found USB device: \n" + title, Toast.LENGTH_SHORT).show();
        }
    }

    private void onDeviceStateChange() {
        stopIoManager();
        startIoManager();
    }

    @Override
    protected void onNewMessage(String s) {
        Log.d(TAG, s);
        String[] parts = s.split("/");
        switch(parts[1]){
            case "start_all":
                break;
            case "stop_all":
                break;
            case "fps":
                fps = Integer.parseInt(parts[2]);
                break;
            case "stop":
                stop();
                break;
            case "m5_drive":
                if(parts.length > 2)
                    drive(parts[2], Float.parseFloat(parts[3]));
                break;
            case "m5_turn":
                if(parts.length > 2)
                    turn(parts[2], Float.parseFloat(parts[3]));
                break;
            case "m5_dcmotor":
                if(parts.length > 2)
                    motor(parts[2], Float.parseFloat(parts[3]));
                break;
            case "m5_fan":
                if(parts.length > 1){
                    pan(Float.parseFloat(parts[2]));
                }
            default :
                break;
        }
    }
    private void makeData(int addr, float velocity, float angle, byte[] data){
        data[0] = (byte) ((addr & 0x000000ff));
        data[1] = (byte) ((addr & 0x0000ff00)>>8);
        data[2] = (byte) ((addr & 0x00ff0000)>>16);
        data[3] = (byte) ((addr & 0xff000000)>>24);
        // velocity
        int bits = Float.floatToIntBits(velocity);
        data[4] = (byte) ((bits & 0x000000ff));
        data[5] = (byte) ((bits & 0x0000ff00)>>8);
        data[6] = (byte) ((bits & 0x00ff0000)>>16);
        data[7] = (byte) ((bits & 0xff000000)>>24);
        // angle
        int angl = Float.floatToIntBits(angle);
        data[8]  = (byte) ((angl & 0x000000ff));
        data[9]  = (byte) ((angl & 0x0000ff00)>>8);
        data[10] = (byte) ((angl & 0x00ff0000)>>16);
        data[11] = (byte) ((angl & 0xff000000)>>24);

    }
    private void sendPackage(int op1, int op2, byte[] data) {

        byte[] buffer = new byte[100];
        int idx = 0;
        buffer[idx++] = (byte) 0xFE;
        buffer[idx++] = 0;
        buffer[idx++] = 0;      // length1
        buffer[idx++] = (byte) op1;   // opcode1
        buffer[idx++] = (byte) op2;

        // escape 0xfe code
        for (int i = 0; i < 12; i++){
            if (data[i] != 0xFE)
                buffer[idx++] = data[i];
            else if (data[i] == 0xFE){	// to escape 0xFE code
                buffer[idx++] = data[i];
                buffer[idx++] = data[i];
            }
        }

        // setup length
        int length = idx - 2; // 2: STX+increment of i
        byte length2 = 0;
        byte length1;
        if(length > 0xFC)
            length2 = (byte) (length - 0xFC);
        length1 = (byte) length;
        buffer[1] = length2;
        buffer[2] = length1;

        // calculation crc
        byte CRC = 0;
        for (int i = 3; i < idx; i++){
            CRC ^= buffer[i];
        }
        CRC |= 0x01; // remove oxFE

        buffer[idx++] = CRC;
        buffer[idx++] = (byte) 0xFD;

        try {
            if (sDriver != null)
                sDriver.write(buffer, 0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // -0.50 ~ +0.50
    private void drive(String direct, float speed) {
        byte[] buffer = new byte[12];
        if(direct.equals("forward")){
            makeData(Protocol.W1_Target, speed, 0, buffer);
            sendPackage(0x01, 0x01, buffer);
            makeData(Protocol.W2_Target, speed, 0, buffer);
            sendPackage(0x01, 0x01, buffer);
        }else{
            makeData(Protocol.W1_Target, -speed, 0, buffer);
            sendPackage(0x01, 0x01, buffer);
            makeData(Protocol.W2_Target, -speed, 0, buffer);
            sendPackage(0x01, 0x01, buffer);
        }
    }
    private void turn(String turn, float speed) {
        byte[] buffer = new byte[12];
        if(turn.equals("left")){
            makeData(Protocol.W1_Target, -speed, 0, buffer);
            sendPackage(0x01, 0x01, buffer);
            makeData(Protocol.W2_Target, speed, 0, buffer);
            sendPackage(0x01, 0x01, buffer);
        }else{
            makeData(Protocol.W1_Target, speed, 0, buffer);
            sendPackage(0x01, 0x01, buffer);
            makeData(Protocol.W2_Target, -speed, 0, buffer);
            sendPackage(0x01, 0x01, buffer);
        }
    }
    private void stop() {
        byte[] buffer = new byte[12];
        makeData(Protocol.W1_Target, 0, 0, buffer);
        sendPackage(0x01, 0x01, buffer);
        makeData(Protocol.W2_Target, 0, 0, buffer);
        sendPackage(0x01, 0x01, buffer);
    }

    private void motor(String wheel, float speed) {
        byte[] buffer = new byte[12];
        if(wheel.equals("1")){
            makeData(Protocol.W1_Target, speed, 0, buffer);
            sendPackage(0x01, 0x01, buffer);
        }else{
            makeData(Protocol.W2_Target, speed, 0, buffer);
            sendPackage(0x01, 0x01, buffer);
        }
    }
    private void pan(float angle){
        byte[] buffer = new byte[12];
        makeData(Protocol.Fan_Reservation, 0x1000, 0, buffer);
        sendPackage(0x01, 0x01, buffer);
        makeData(Protocol.F_Target, 0, angle, buffer);
        sendPackage(0x01, 0x01, buffer);
    }
}
