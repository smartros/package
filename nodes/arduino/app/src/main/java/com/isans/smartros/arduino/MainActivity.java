package com.isans.smartros.arduino;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import org.ros.android.smartRosActivity;
import org.shokai.firmata.ArduinoFirmata;
import org.shokai.firmata.ArduinoFirmataEventHandler;

import java.io.IOException;
import java.util.Timer;


public class MainActivity extends smartRosActivity {

    private String TAG = "MainActivity";
    private Handler handler;
    // ref: https://github.com/shokai/ArduinoFirmata-Android
    private ArduinoFirmata arduino = null;
    private int fps = 1;
    private byte[] firmataPinModes={ArduinoFirmata.INPUT,ArduinoFirmata.OUTPUT,ArduinoFirmata.ANALOG,ArduinoFirmata.PWM,ArduinoFirmata.SERVO };
    private String[] a4sPinModes = {"Digital Input", "Digital Output","Analog Input","Analog Output(PWM)","Servo"};
    public MainActivity(){
        super("Arduino Node");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // user code here
        this.handler = new Handler();
        Log.v(TAG, "start");
        Log.v(TAG, "Firmata Lib Version : " + ArduinoFirmata.VERSION);
        this.arduino = new ArduinoFirmata(this);
        final Activity self = this;
        arduino.setEventHandler(new ArduinoFirmataEventHandler() {
            public void onError(String errorMessage) {
                Log.e(TAG, errorMessage);
            }
            public void onClose() {
                Log.v(TAG, "arduino closed");
                self.finish();
            }
        });
        Thread thread = new Thread(new Runnable(){
            public void run(){

                while(arduino.isOpen()){
                    try{
                        Thread.sleep(1000/fps);
                        handler.post(new Runnable(){
                            public void run(){
                                String message = new String();
                                for (int i = 2; i <= 13; i++) {
                                    message += "digitalRead/" + i + " " + (arduino.digitalRead(i) == ArduinoFirmata.HIGH ? "true" : "false") + "\n";
                                }
                                for (int i = 0; i <= 5; i++) {
                                    message += "analogRead/" + i + " " + arduino.analogRead(i) + "\n";
                                }
                                sendRespMessage(message);
                            }
                        });
                    } catch(InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        try{
            arduino.connect();
            Log.v(TAG, "Board Version : " + arduino.getBoardVersion());
            Toast.makeText(getApplicationContext(), "Arduino is connected", Toast.LENGTH_SHORT).show();
            arduino.pinMode(7, ArduinoFirmata.INPUT);
            thread.start();
        }
        catch(IOException e){
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Connection to arduino is failed", Toast.LENGTH_SHORT).show();
            finish();
        }
        catch(InterruptedException e){
            e.printStackTrace();
            finish();
        }

        moveTaskToBack(true);
    } // onCreate

    @Override
    protected void onNewMessage(String s) {
        Log.d(TAG, s);
        String[] parts = s.split("/");
        // ex parts[0] = "", parts[1] = "pinMode" ...
        switch(parts[1]){
            case "start_all":   // system
                break;
            case "stop_all":    // system
                break;
            case "fps":         // system
                fps = Integer.parseInt(parts[2]);
                break;
            case "pinMode":
                if(parts.length > 3 ) {
                    byte b = getFirmataPinMode(parts[3]);
                    arduino.pinMode(Integer.parseInt(parts[2]), b);
                }
                break;
            case "digitalWrite":
                if(parts.length > 3 )
                    arduino.digitalWrite(Integer.parseInt(parts[2]),
                        "high".equals(parts[3]) ? ArduinoFirmata.HIGH : ArduinoFirmata.LOW);
                break;
            case "analogWrite":
                if(parts.length > 3 )
                    arduino.analogWrite(Integer.parseInt(parts[2]), Integer.parseInt(parts[3]));
                break;
            case "servoWrite":
                if(parts.length > 3 )
                    arduino.servoWrite(Integer.parseInt(parts[2]), Integer.parseInt(parts[3]));
                break;
            default:
                break;
        }
    }
    private byte getFirmataPinMode(String a4sPinMode){
        int idx=0;
        while (idx < a4sPinModes.length-1 && (! a4sPinMode.equals(a4sPinModes[idx]))) idx++;
        if (! a4sPinMode.equals(a4sPinModes[idx]) ) idx=0;
        return firmataPinModes[idx];
    }
}
