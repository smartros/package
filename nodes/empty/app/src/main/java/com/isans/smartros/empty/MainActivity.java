package com.isans.smartros.empty;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import org.ros.android.smartRosActivity;

public class MainActivity extends smartRosActivity {

    public MainActivity(){
        super("Empty Node");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        moveTaskToBack(true);   // turn background task
    }
    @Override
    protected void onNewMessage(String s) {
        String [] parts = s.split("/");
        Log.d("test", s);
        switch(parts[1]) {
            case "em_send_message":
                sendRespMessage(String.format("em_get_message %s\n", parts[2]));
                break;
        }
    }
}
