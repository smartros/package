package com.isans.smartros.parrot;


import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.parrot.arsdk.arcommands.ARCOMMANDS_MINIDRONE_ANIMATIONS_FLIP_DIRECTION_ENUM;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceBLEService;
import com.parrot.arsdk.ardiscovery.ARDiscoveryDeviceService;
import com.parrot.arsdk.ardiscovery.ARDiscoveryService;
import com.parrot.arsdk.ardiscovery.receivers.ARDiscoveryServicesDevicesListUpdatedReceiver;
import com.parrot.arsdk.ardiscovery.receivers.ARDiscoveryServicesDevicesListUpdatedReceiverDelegate;
import com.parrot.arsdk.arsal.ARSALPrint;

import org.ros.android.smartRosActivity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends smartRosActivity
        implements ARDiscoveryServicesDevicesListUpdatedReceiverDelegate {

    private static final String TAG = "MainActivity";

    public MainActivity() {
        super("Parrot Node");
    }

    private long startTime = 0;
    private List<ARDiscoveryDeviceService> deviceList;
    public DeviceController deviceController = null;
    public ARDiscoveryDeviceService service;
    private ARDiscoveryService ardiscoveryService;
    private boolean ardiscoveryServiceBound = false;
    private ServiceConnection ardiscoveryServiceConnection;
    public IBinder discoveryServiceBinder;

    private BroadcastReceiver ardiscoveryServicesDevicesListUpdatedReceiver;

    static {
        try {
            System.loadLibrary("arsal");
            System.loadLibrary("arsal_android");
            System.loadLibrary("arnetworkal");
            System.loadLibrary("arnetworkal_android");
            System.loadLibrary("arnetwork");
            System.loadLibrary("arnetwork_android");
            System.loadLibrary("arcommands");
            System.loadLibrary("arcommands_android");
            System.loadLibrary("ardiscovery");
            System.loadLibrary("ardiscovery_android");
            ARSALPrint.enableDebugPrints();
        } catch (Exception e) {
            Log.e(TAG, "Oops (LoadLibrary)", e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        moveTaskToBack(true);   // turn background task
        startServices();
        initBroadcastReceiver();
        initServiceConnection();
        deviceList = new ArrayList<>();
    }

    @Override
    protected void onNewMessage(String s) {
        Log.d(TAG, s);
        long endTime = System.currentTimeMillis();
        long duration;
        String[] parts = s.split("/");
        switch (parts[1]) {
            case "start_all":
                onServicesDevicesListUpdated();
                registerReceivers();
                initServices();
                break;
            case "reset_all":
                break;
            case "fps":
                break;
            case "pa_start":
                if (deviceList.size() > 0) {
                    service = deviceList.get(0);
                    if (service != null) {
                        if( deviceController != null) break;

                        deviceController = new DeviceController(this, service);
                        deviceController.setListener(new DeviceControllerListener() {
                            @Override
                            public void onDisconnect() {
                                stopDeviceController();
                                deviceController = null;
                            }

                            @Override
                            public void onUpdateBattery(final byte percent) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        // show battery percent
                                        sendRespMessage("pa_get_battery " + percent + "\n");
                                    }
                                });
                            }
                        });
                        startDeviceController();
                    }
                }
                break;
            case "pa_takeoff":
                if (deviceController != null) {
                    deviceController.sendTakeoff();
                }
                break;
            case "pa_landing":
                if (deviceController != null) {
                    deviceController.sendLanding();
                }
                break;
            case "pa_stop":
                if (deviceController != null) {
                    deviceController.setYaw((byte) 0);
                    deviceController.setPitch((byte) 0);
                    deviceController.setRoll((byte) 0);
                    deviceController.setFlag((byte)0);
                    deviceController.setGaz((byte)0);
                }
                break;
            case "pa_turn":
                if (deviceController != null) {
                    if (parts[2].equals("left")) {
                        deviceController.setYaw((byte) -50);
                    } else if (parts[2].equals("right")) {
                        deviceController.setYaw((byte) 50);
                    }
                }
                break;
            case "pa_emergency":
                if (deviceController != null) {
                    deviceController.sendEmergency();
                }
                break;
            case "pa_control":
                if (deviceController != null) {
                    if (parts[2].equals("forward")) {
                        deviceController.setPitch((byte) 50);
                        deviceController.setFlag((byte) 1);
                    } else if (parts[2].equals("backward")) {
                        deviceController.setPitch((byte) -50);
                        deviceController.setFlag((byte) 1);
                    } else if (parts[2].equals("left")) {
                        deviceController.setRoll((byte) -50);
                        deviceController.setFlag((byte) 1);
                    } else if (parts[2].equals("right")) {
                        deviceController.setRoll((byte) 50);
                        deviceController.setFlag((byte) 1);
                    } else if (parts[2].equals("up")) {
                        deviceController.setGaz((byte) 50);
                    } else if (parts[2].equals("down")) {
                        deviceController.setGaz((byte) -50);
                    }
                }
                break;
            case "pa_flip":
                if (deviceController != null) {
                    if (parts[2].equals("front")) {
                        deviceController.flip(ARCOMMANDS_MINIDRONE_ANIMATIONS_FLIP_DIRECTION_ENUM.ARCOMMANDS_MINIDRONE_ANIMATIONS_FLIP_DIRECTION_FRONT);
                    } else if (parts[2].equals("back")) {
                        deviceController.flip(ARCOMMANDS_MINIDRONE_ANIMATIONS_FLIP_DIRECTION_ENUM.ARCOMMANDS_MINIDRONE_ANIMATIONS_FLIP_DIRECTION_BACK);
                    } else if (parts[2].equals("left")) {
                        deviceController.flip(ARCOMMANDS_MINIDRONE_ANIMATIONS_FLIP_DIRECTION_ENUM.ARCOMMANDS_MINIDRONE_ANIMATIONS_FLIP_DIRECTION_LEFT);
                    } else if (parts[2].equals("right")) {
                        deviceController.flip(ARCOMMANDS_MINIDRONE_ANIMATIONS_FLIP_DIRECTION_ENUM.ARCOMMANDS_MINIDRONE_ANIMATIONS_FLIP_DIRECTION_RIGHT);
                    }
                }
                break;
            default:
                break;
        }
    }

    private void startServices() {
        //startService(new Intent(this, ARDiscoveryService.class));
    }

    private void initServices() {
        if (discoveryServiceBinder == null) {
            Intent i = new Intent(getApplicationContext(), ARDiscoveryService.class);
            getApplicationContext().bindService(i, ardiscoveryServiceConnection, Context.BIND_AUTO_CREATE);
        } else {
            ardiscoveryService = ((ARDiscoveryService.LocalBinder) discoveryServiceBinder).getService();
            ardiscoveryServiceBound = true;
            ardiscoveryService.start();
        }
    }

    private void closeServices() {
        Log.d(TAG, "closeServices ...");
        if (ardiscoveryServiceBound) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    ardiscoveryService.stop();
                    getApplicationContext().unbindService(ardiscoveryServiceConnection);
                    ardiscoveryServiceBound = false;
                    discoveryServiceBinder = null;
                    ardiscoveryService = null;
                }
            }).start();
        }
    }

    private void initBroadcastReceiver() {
        ardiscoveryServicesDevicesListUpdatedReceiver = new ARDiscoveryServicesDevicesListUpdatedReceiver(this);
    }

    private void initServiceConnection() {
        ardiscoveryServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                discoveryServiceBinder = service;
                ardiscoveryService = ((ARDiscoveryService.LocalBinder) service).getService();
                ardiscoveryServiceBound = true;
                ardiscoveryService.start();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                ardiscoveryService = null;
                ardiscoveryServiceBound = false;
            }
        };
    }

    private void registerReceivers() {
        LocalBroadcastManager localBroadcastMgr = LocalBroadcastManager.getInstance(getApplicationContext());
        localBroadcastMgr.registerReceiver(ardiscoveryServicesDevicesListUpdatedReceiver, new IntentFilter(ARDiscoveryService.kARDiscoveryServiceNotificationServicesDevicesListUpdated));

    }

    private void unregisterReceivers() {
        LocalBroadcastManager localBroadcastMgr = LocalBroadcastManager.getInstance(getApplicationContext());
        localBroadcastMgr.unregisterReceiver(ardiscoveryServicesDevicesListUpdatedReceiver);
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume ...");
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy ...");
        unregisterReceivers();
        closeServices();
        super.onPause();
    }

    @Override
    public void onServicesDevicesListUpdated() {
        Log.d(TAG, "onServicesDevicesListUpdated ...");
        if(deviceList.size() > 0 ) return ;

        sendRespMessage("pa_get_device 1\n");
        sendRespMessage("pa_started false\n");
        List<ARDiscoveryDeviceService> list;
        if (ardiscoveryService != null) {
            list = ardiscoveryService.getDeviceServicesArray();
            deviceList = new ArrayList<>();
            List<String> deviceNames = new ArrayList<>();
            if (list != null) {
                for (ARDiscoveryDeviceService service : list) {
                    Log.d(TAG, "service :  " + service);
                    sendRespMessage("pa_get_device " + service + "\n");
                    if (service.getDevice() instanceof ARDiscoveryDeviceBLEService) {
                        deviceList.add(service);
                        deviceNames.add(service.getName());
                    }
                }
            }
        }
    }

    private void startDeviceController() {
        if (deviceController != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean failed;
                    failed = deviceController.start();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Parrot is connected", Toast.LENGTH_SHORT).show();
                            sendRespMessage("pa_started true\n");
                        }
                    });
                    if (failed) {
                        finish();
                    } else {
                        //only with RollingSpider in version 1.97 : date and time must be sent to permit a reconnection
                        Date currentDate = new Date(System.currentTimeMillis());
                        deviceController.sendDate(currentDate);
                        deviceController.sendTime(currentDate);
                    }
                }
            }).start();
        }
    }

    private void stopDeviceController() {
        if (deviceController != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        deviceController.stop();
                    } catch (Exception e) {
                        Log.d(TAG, "deviceController.stop() error");
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Parrot is disconnected", Toast.LENGTH_SHORT).show();
                            sendRespMessage("pa_started false\n");
                            finish();
                        }
                    });
                }
            }).start();
        }
    }
}
