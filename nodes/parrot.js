(function (ext) {

	ext._shutdown = function() {};
	ext._getStatus = function() {
		return { status:2, msg: 'Ready' };
	};
	var descriptor = {
		blocks: [
				[" ", "start", "pa_start"],
				["r", "get device", "pa_get_device"],
				["r", "get battery", "pa_get_battery"],
				["b", "is started", "pa_started", "false"],
				[" ", "takeoff", "pa_takeoff"],
				[" ", "landing", "pa_landing"],		
				[" ", "stop", "pa_stop"],
				[" ", "move %m.control", "pa_control", "forward"],
				[" ", "turn %m.turn", "pa_turn", "left"],
				[" ", "flip %m.flip", "pa_flip", "front"],
				[" ", "emergency", "pa_emergency"],
        ],
		menus: {			
			"control":["forward", "backward", "left", "right", "up", "down"],
			"turn":["left", "right"],
			"flip":["front", "back"],
		},
		url: 'http://cafe.naver.com/smartros',
		port: 50007,
		node: 'com.isans.smartros.parrot'
		
	};
	ScratchExtensions.register('Parrot', descriptor, ext);
})({});