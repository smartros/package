(function (ext) {

	ext._shutdown = function() {};
	ext._getStatus = function() {
		return { status:2, msg: 'Ready' };
	};
	var descriptor = {
		blocks: [
				[" ", "text to speech %s", "sr_tts"],
				["r", "get %m.info of %m.recog", "recog_info", "x", "marker"],			
				[" ", "%m.turn %m.recog detection", "sr_recognition", "stop", "marker"],	
				[" ", "take picture", "sr_take_picture"],
				[" ", "start %s application", "sr_start_package"],				
				[" ", "open YouTube %s", "sr_youtube", "taylorswift"],				
        ],
		menus: {			
			"recog": ["marker", "face", "speech"],
			"turn": ["start", "stop"],
			"info": ["x", "y", "id", "size", "word"],			
		},
		url: 'http://cafe.naver.com/smartros',
		port: 50007,
		node: 'com.isans.smartros.android'
		
	};
	ScratchExtensions.register('Android', descriptor, ext);
})({});