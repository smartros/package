(function (ext) {

	ext._shutdown = function() {};
	ext._getStatus = function() {
		return { status:2, msg: 'Ready' };
	};
	var descriptor = {
		blocks: [
				[" ", "go %m.drive speed %n", "mb_drive", "forward", 50],
				[" ", "turn %m.direct speed %n", "mb_turn", "left", 50],
				[" ", "stop"],
				[" ", "run motor %m.motorPort speed %n", "mb_dcmotor", "M1", 50],			
				["r", "ultrasonic sensor %m.normalPort distance", "mb_sonar", "Port3"],
        ],
		menus: {			
			"drive":["forward", "backward"],
			"direct":["left", "right"],
			"normalPort":["Port3"],
			"motorPort":["M1","M2"],
			
		},
		url: 'http://cafe.naver.com/smartros',
		port: 50007,
		node: 'com.isans.smartros.makeblock'
		
	};
	ScratchExtensions.register('Makeblock', descriptor, ext);
})({});