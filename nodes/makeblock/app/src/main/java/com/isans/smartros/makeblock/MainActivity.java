package com.isans.smartros.makeblock;

import android.content.Context;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.HexDump;
import com.hoho.android.usbserial.util.SerialInputOutputManager;
import org.ros.android.smartRosActivity;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// ref: https://github.com/Makeblock-official/Makeblock-Library/blob/master/makeblock/Makeblock.cpp
// ref: https://code.google.com/p/usb-serial-for-android/source/browse/UsbSerialExamples/src
// /com/hoho/android/usbserial/examples/SerialConsoleActivity.java
public class MainActivity extends smartRosActivity {

    class Module{
        protected int module;
        protected int port;
        protected int slot;
        protected int pin;
        protected float[] value = new float[4];
    }

    private static UsbSerialDriver sDriver = null;
    private UsbManager mUsbManager;
    private byte[] rx = new byte[128];
    private int rxIndex = 0;
    private int fps = 1;
    private Handler handler;
    private static final String TAG = "MainActivity";
    public MainActivity() {
        super("Makeblock Node");
    }
    private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();

    private SerialInputOutputManager mSerialIoManager;

    private final SerialInputOutputManager.Listener mListener =
        new SerialInputOutputManager.Listener() {
            @Override
            public void onRunError(Exception e) {
                Log.d(TAG, "Runner stopped.");
                sDriver = null;
            }
            @Override
            public void onNewData(final byte[] data) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(data.length >= 3 ){
                            System.arraycopy(data, 0, rx, rxIndex, data.length);
                            if(rx[0] == (byte)0xff && rx[1] == 0x55 && rx[2] == 0x01){
                                rxIndex += data.length;
                                if( rxIndex >=7 ) {
                                    byte[] b = new byte[]{rx[6], rx[5], rx[4], rx[3]};
                                    float t = ByteBuffer.wrap(b).getFloat();
                                    if( t >= 0 && t < 300 ) { // fixed range
                                        sendRespMessage(String.format("mb_sonar/Port3 %f\n", t));
                                    }
                                    rxIndex = 0;
                                }
                            }
                        }
                    }
                });
            }
        };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        moveTaskToBack(true);   // turn background task
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        sDriver = UsbSerialProber.acquire(mUsbManager);
        this.handler = new Handler();
        Thread thread = new Thread(new Runnable() {
            public void run() {

                while (sDriver != null) {
                    try {
                        Thread.sleep(1000 / fps);
                        handler.post(new Runnable() {
                            public void run() {
                                byte[] test = new byte[6];
                                test[0] = (byte) 0xff;
                                test[1] = 0x55;
                                test[2] = 0x01;
                                test[3] = 0x02;
                                test[4] = Protocol.ULTRASONIC_SENSOR;
                                test[5] = Protocol.PORT3 + Protocol.SLOT1;
                                try {
                                    if(sDriver != null)
                                        sDriver.write(test, 0);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "Resumed, sDriver=" + sDriver);
        if (sDriver == null) {
            Log.d(TAG, "No serial device.");
        } else {
            try {
                sDriver.open();
                sDriver.setBaudRate(115200);// , 8, UsbSerialDriver.STOPBITS_1, UsbSerialDriver.PARITY_NONE);
            } catch (IOException e) {
                Log.e(TAG, "Error setting up device: " + e.getMessage(), e);
                try {
                    sDriver.close();
                } catch (IOException e2) {
                    // Ignore.
                }
                sDriver = null;
                return;
            }
            Log.d(TAG, "Serial device: " + sDriver.getClass().getSimpleName());
        }
        onDeviceStateChange();
    }
    private void stopIoManager() {
        if (mSerialIoManager != null) {
            Log.i(TAG, "Stopping io manager ..");
            mSerialIoManager.stop();
            mSerialIoManager = null;
        }
    }

    private void startIoManager() {
        if (sDriver != null) {
            Log.i(TAG, "Starting io manager ..");
            mSerialIoManager = new SerialInputOutputManager(sDriver, mListener);
            mExecutor.submit(mSerialIoManager);
            final String title = String.format("Vendor %s Product %s",
                    HexDump.toHexString((short) sDriver.getDevice().getVendorId()),
                    HexDump.toHexString((short) sDriver.getDevice().getProductId()));
            Toast.makeText(this, "Found USB device: \n" + title, Toast.LENGTH_SHORT).show();
        }
    }

    private void onDeviceStateChange() {
        stopIoManager();
        startIoManager();
    }
    private Module constructModule(int module, int port, int slot, int pin, int value){
        Module mod = new Module();
        mod.module = module;
        mod.port = port;
        mod.slot = slot;
        mod.pin = pin;
        mod.value[0] = value;
        return mod;
    }
    private void deviceRun(Module mod){
        if(sDriver == null) return ;
        // ff 55 2 dev port|slot value[4];
        byte[] cc = new byte[10];
        cc[0] = (byte) 0xff;
        cc[1] = 0x55;
        cc[2] = 0x02;
        cc[3] = 0x06;
        cc[4] = (byte) mod.module;
        cc[5] = (byte) (mod.module >= Protocol.DIGITAL_INPUT?mod.pin:(mod.port+mod.slot));
        int bits = Float.floatToIntBits(mod.value[0]);
        cc[6] = (byte) ((bits & 0x000000ff));
        cc[7] = (byte) ((bits & 0x0000ff00)>>8);
        cc[8] = (byte) ((bits & 0x00ff0000)>>16);
        cc[9] = (byte) ((bits & 0xff000000)>>24);
        try {
            if(sDriver != null)
                sDriver.write(cc, 0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onNewMessage(String s) {
        Log.d(TAG, s);
        String[] parts = s.split("/");
        switch(parts[1]){
            case "start_all":
                break;
            case "reset_all":
                break;
            case "fps":
                fps = Integer.parseInt(parts[2]);
                break;
            case "mb_drive":
                if(parts.length > 2)
                    drive(parts[2], Integer.parseInt(parts[3]));
                break;
            case "mb_turn":
                if(parts.length > 2)
                    turn(parts[2], Integer.parseInt(parts[3]));
                break;
            case "mb_dcmotor":
                if(parts.length > 2)
                    motor(parts[2], Integer.parseInt(parts[3]));
                break;
            default :
                break;
        }
    }

    private void drive(String direct, int speed) {
        Module mod;
        if(direct.equals("forward")){
            mod = constructModule(Protocol.MOTOR, Protocol.M1, Protocol.SLOT1, 0, speed);
            deviceRun(mod);
            mod = constructModule(Protocol.MOTOR, Protocol.M2, Protocol.SLOT1, 0, speed);
            deviceRun(mod);
        }
        else{
            mod = constructModule(Protocol.MOTOR, Protocol.M1, Protocol.SLOT1, 0, -speed);
            deviceRun(mod);
            mod = constructModule(Protocol.MOTOR, Protocol.M2, Protocol.SLOT1, 0, -speed);
            deviceRun(mod);
        }
    }
    private void turn(String turn, int speed) {
        Module mod;
        if(turn.equals("left")){
            mod = constructModule(Protocol.MOTOR, Protocol.M1, Protocol.SLOT1, 0, speed);
            deviceRun(mod);
            mod = constructModule(Protocol.MOTOR, Protocol.M2, Protocol.SLOT1, 0, -speed);
            deviceRun(mod);
        }
        else{
            mod = constructModule(Protocol.MOTOR, Protocol.M1, Protocol.SLOT1, 0, -speed);
            deviceRun(mod);
            mod = constructModule(Protocol.MOTOR, Protocol.M2, Protocol.SLOT1, 0, speed);
            deviceRun(mod);
        }
    }
    private void motor(String port, int speed) {
        Module mod = constructModule(Protocol.MOTOR, str2motor(port), Protocol.SLOT1, 0, speed);
        deviceRun(mod);
    }

    private int str2motor(String port){
        if(port.equals("M1")) return Protocol.M1;
        if(port.equals("M2")) return Protocol.M2;
        if(port.equals("Port1")) return Protocol.PORT1;
        if(port.equals("Port2")) return Protocol.PORT2;
        return 0;
    }
}
