package com.isans.smartros.makeblock;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bcc on 2015-06-05.
 */
public class Protocol {
    public static final int SLOT1 = 1;
    public static final int SLOT2 = 2;

    public static final int AXISX = 0;
    public static final int AXISY = 1;
    public static final int AXISZ = 2;

    public static final int PORT1 = 0x10;
    public static final int PORT2 = 0x20;
    public static final int PORT3 = 0x30;
    public static final int PORT4 = 0x40;
    public static final int PORT5 = 0x50;
    public static final int PORT6 = 0x60;
    public static final int PORT7 = 0x70;
    public static final int PORT8 = 0x80;
    public static final int M1 = 0x90;
    public static final int M2 = 0xA0;
    public static final int I2C =  0xB0;
    public static final int DIGIPORT = 0xC0;
    public static final int ALOGPORT = 0xD0;

    public static final int VERSION = 0;
    public static final int ULTRASONIC_SENSOR = 1;
    public static final int TEMPERATURE_SENSOR = 2;
    public static final int LIGHT_SENSOR = 3;
    public static final int POTENTIONMETER = 4;
    public static final int JOYSTICK = 5;
    public static final int GYRO = 6;
    public static final int RGBLED = 8;
    public static final int SEVSEG = 9;
    public static final int MOTOR = 10;
    public static final int SERVO = 11;
    public static final int ENCODER = 12;
    public static final int INFRARED = 16;
    public static final int LINEFOLLOWER = 17;

    public static final int DIGITAL_INPUT = 30;
    public static final int ANALOG_INPUT = 31;
    public static final int DIGITAL_OUTPUT = 32;
    public static final int ANALOG_OUTPUT = 33;
    public static final int PWM_OUTPUT = 34;

}
