package com.isans.smartros.altino;

import android.content.Context;
import android.hardware.usb.UsbManager;
import android.os.*;
import android.util.Log;
import android.widget.Toast;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.HexDump;
import com.hoho.android.usbserial.util.SerialInputOutputManager;

import org.ros.android.smartRosActivity;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;


public class MainActivity extends smartRosActivity {

    private static UsbSerialDriver sDriver = null;
    private UsbManager mUsbManager;
    private int fps = 1;
    private boolean bSend = false;
    private static final String TAG = MainActivity.class.getSimpleName();
    private Handler handler;
    private Protocol altino = null;
    public MainActivity() {
        super("Altino Node");
    }
    private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();
    private SerialInputOutputManager mSerialIoManager;

    private final SerialInputOutputManager.Listener mListener =
            new SerialInputOutputManager.Listener() {
                @Override
                public void onRunError(Exception e) {
                    Log.d(TAG, "Runner stopped.");
                    sDriver = null;
                }
                @Override
                public void onNewData(final byte[] data) {
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // parsing code
                            altino.receiveData(data);
                        }
                    });
                }
            };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        moveTaskToBack(true);   // turn background task
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        sDriver = UsbSerialProber.acquire(mUsbManager);
        altino = new Protocol(sDriver);
        this.handler = new Handler();
        Thread thread = new Thread(new Runnable() {
            public void run() {

                while (sDriver != null) {
                    try {
                        Thread.sleep(1000 / fps);    // fixed fps, because data error
                        handler.post(new Runnable() {
                            public void run() {
                                // periodic writing code
                                StringBuffer recordValue = new StringBuffer();
                                altino.recvRespMessage(recordValue);
                                if( bSend ) {

                                }
                                sendRespMessage(recordValue.toString());
                            }
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "Resumed, sDriver=" + sDriver);
        if (sDriver == null) {
            Log.d(TAG, "No serial device.");
        } else {
            try {
                sDriver.open();
                sDriver.setBaudRate(115200);// , 8, UsbSerialDriver.STOPBITS_1, UsbSerialDriver.PARITY_NONE);
            } catch (IOException e) {
                Log.e(TAG, "Error setting up device: " + e.getMessage(), e);
                try {
                    sDriver.close();
                } catch (IOException e2) {
                    // Ignore.
                }
                sDriver = null;
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        onDeviceStateChange();
    }
    private void stopIoManager() {
        if (mSerialIoManager != null) {
            Log.i(TAG, "Stopping io manager ..");
            mSerialIoManager.stop();
            mSerialIoManager = null;
        }
    }

    private void startIoManager() throws Exception {
        if (sDriver != null) {
            Log.i(TAG, "Starting io manager ..");
            mSerialIoManager = new SerialInputOutputManager(sDriver, mListener);
            mExecutor.submit(mSerialIoManager);
            final String title = String.format("Vendor %s Product %s",
                    HexDump.toHexString((short) sDriver.getDevice().getVendorId()),
                    HexDump.toHexString((short) sDriver.getDevice().getProductId()));
            Toast.makeText(this, "Found USB device: \n" + title, Toast.LENGTH_SHORT).show();
        }
    }

    private void onDeviceStateChange() {
        stopIoManager();
        try {
            startIoManager();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        bSend = false;
        stopIoManager();
    }

    @Override
    protected void onNewMessage(String s) {
        String[] parts = s.split("/");
        if (parts.length == 0) return ;
        switch (parts[1]) {
            case "start_all":
                bSend = true;
                Log.d(TAG, "start_all");
                break;
            case "reset_all":
                Log.d(TAG, "reset_all");
                bSend = false;
                break;
            case "fps":
                fps = Integer.parseInt(parts[2]);
                break;
            case "led":
                altino.setLED(parts);
            case "dot":
                altino.setDotMatrix(parts);
                break;
            case "ascii":
                altino.setAscii(parts);
                break;
            case "motor":
                altino.setMotor(parts);
                break;
            case "backmotor":
                altino.setbackMotor(parts);
                break;
            case "buzzer":
                altino.setBuzzer(parts);
                break;
            case "go":
                altino.setDriver(parts);
                break;
            case "wheel":
                altino.setWheel(parts);
                break;
            case "stop":
                altino.stopWheel();
                break;
            case "setturn":
                altino.setTurn(parts);
            default:
                break;
        }
    }

}