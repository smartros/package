package com.isans.smartros.altino;

import android.os.Environment;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialDriver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Semaphore;

/**
 * Created by bcc on 2015-05-28.
 */
public class Protocol {


    Semaphore mutex = new Semaphore(1);

    private static UsbSerialDriver sDriver = null;
    // buffer
    private byte[] rx_d = new byte[31];
    private byte[] readBuf_byte = new byte[31];
    private byte[] tx_d = new byte[28];
    private byte[] sendBuf_byte = new byte[28];
    private boolean recv_flag = false;
    private int index = 0;
    private static final String TAG = Protocol.class.getSimpleName();
    private String logname = "";
    private FileWriter out = null;
    Protocol(UsbSerialDriver driver) {
        sDriver = driver;
        if(sDriver != null) {
            sDriver.setWriteBufferSize(28);
        }
    }
    public void appendLog(String text) {
        File logFile = new File("sdcard/log.txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try
        {
            SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss");
            String format = s.format(new Date());

            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append("["+format+"]"+text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public boolean isPacket(byte[] readbuf, int length){
        return (readbuf[0] == 0x02 && length == 31);
    }
    public static String byteArrayToHex(byte[] ba) {
        if (ba == null || ba.length == 0) {
            return null;
        }

        StringBuffer sb = new StringBuffer(ba.length * 2);
        String hexNumber;
        for (int x = 0; x < ba.length; x++) {
            hexNumber = "0" + Integer.toHexString(0xff & ba[x]);

            sb.append(hexNumber.substring(hexNumber.length() - 2));
        }
        return sb.toString();
    }
    // 31, 5, 26, 20, 11, 3, 59, 1, 61, 2, 60
    public void receiveData(byte[] readbuf){

        // 1. appendLog(String.valueOf(readbuf.length));
        if( readbuf.length > 0 ){
            if( readbuf.length == 31 ){
                // ok
                byte[] tmp = new byte[31];
                int idx = 0;
                for(byte r : readbuf) {
                    tmp[idx++] = r;
                }
                if(isPacket(tmp, idx)){
                    // copy to origin
                    System.arraycopy(tmp, 0, readBuf_byte, 0, 31);
                    // 1. appendLog(byteArrayToHex(tmp));
                    //appendLog(readbuf.length+" 1");
                }
            }
            /*else if( readbuf.length < 31 ){
                for(byte r : readbuf){
                    rx_d[index++] = r;
                }
                if(isPacket(rx_d, index)){
                    // copy to origin
                    // clear rx_d
                    // reset index

                    index = 0;
                    appendLog(readbuf.length+" 2");
                }else{
                    if(index == 31){
                        // error
                        index = 0;
                        appendLog(readbuf.length+" error 2");
                    }
                }
            }
            */else{
                for(byte r : readbuf){
                    rx_d[index++] = r;
                    if(isPacket(rx_d, index)){
                        // copy to origin
                        System.arraycopy(rx_d, 0, readBuf_byte, 0, 31);
                        // reset index
                        index = 0;
                        //appendLog(readbuf.length + " 3");
                        rx_d = new byte[31];
                    }else{
                        if(index == 31){
                            //error
                            index = 0;
                            //appendLog(readbuf.length+" error 3");
                            // 1. appendLog(byteArrayToHex(rx_d));
                            rx_d = new byte[31];
                        }
                    }
                }
            }
        }
    }
    public void updateReceivedData(byte[] Readbuf) {
        appendLog(String.valueOf(Readbuf.length));
        if (recv_flag) {
            for (byte aReadbuf : Readbuf) {
                rx_d[index] = aReadbuf;
                index++;

                if (index == 31) {
                    recv_flag = false;
                    index = 0;
                    System.arraycopy(rx_d, 0, readBuf_byte, 0, 31);
                    break;
                }
            }
        } else {
            if (index == 0 && Readbuf[0] == 0x02) {
                recv_flag = true;
                for (byte aReadbuf : Readbuf) {
                    rx_d[index] = aReadbuf;
                    index++;

                    if (index == 31) {
                        recv_flag = false;
                        index = 0;
                        System.arraycopy(rx_d, 0, readBuf_byte, 0, 31);
                        break;
                    }
                }
            }
        }
    }

    public final short getData(int offset) { // convert int to short
        return (short) (((readBuf_byte[offset] & 0xff) << 8) | (readBuf_byte[offset + 1] & 0xff));
    }

    public void setLED(String[] params) {
        String param1 = params[2];
        String param2 = params[3];

        if (param1.equals("on")) {
            if (param2.equals("front")) {
                sendBuf_byte[23] = (byte) (0x33); // turn on front led
            } else if (param2.equals("back")) {
                sendBuf_byte[23] = (byte) (0xCC); // turn on back led
            } else if (param2.equals("all")) {
                sendBuf_byte[23] = (byte) (0xFF); // turn on all led
            } else if (param2.equals("left-signal")) {
                sendBuf_byte[23] = (byte) (0x20);
            } else if (param2.equals("right-signal")) {
                sendBuf_byte[23] = (byte) (0x10);
            } else if (param2.equals("signal-all")) {
                sendBuf_byte[23] = (byte) (0x30);
            }
            sendByte(sendBuf_byte);
        } else if (param1.equals("off")) {
            if (param2.equals("front")) {
                sendBuf_byte[23] = (byte) (sendBuf_byte[23] & ((byte) 0xCC));
            } else if (param2.equals("back")) {
                sendBuf_byte[23] = (byte) (sendBuf_byte[23] & ((byte) 0x33));
            } else if (param2.equals("all")) {
                sendBuf_byte[23] = (byte) (0x00);
            } else if (param1.equals("left-signal")) {
                sendBuf_byte[23] = (byte) (sendBuf_byte[23] & ((byte) 0x10));
            } else if (param1.equals("right-signal")) {
                sendBuf_byte[23] = (byte) (sendBuf_byte[23] & ((byte) 0x20));
            } else if (param1.equals("signal-all")) {
                sendBuf_byte[23] = (byte) (0x00);
            }
            sendByte(sendBuf_byte);
        }
    }

    public void setDriver(String[] params) {
        String param1 = params[2];
        String param2 = params[3];
        if (param1.equals("forward")) {
            goForward(param2);
        } else if (param1.equals("backward")) {
            goBackward(param2);
        }
    }

    private void goForward(String params) {
        int speed;
        switch (params) {
            case "slow":
                speed = 260;
                break;
            case "fast":
                speed = 520;
                break;
            default:  // medium
                speed = 400;
                break;
        }
        sendBuf_byte[6] = 0;
        sendBuf_byte[7] = (byte) (speed / 256);
        sendBuf_byte[8] = (byte) (speed % 256);
        sendBuf_byte[9] = 0;
        sendBuf_byte[10] = (byte) (speed / 256);
        sendBuf_byte[11] = (byte) (speed % 256);
        sendByte(sendBuf_byte);
    }

    private void goBackward(String params) {
        int speed;
        switch (params) {
            case "slow":
                speed = 260;
                break;
            case "fast":
                speed = 520;
                break;
            default:  // medium
                speed = 400;
                break;
        }
        sendBuf_byte[6] = 0;
        int BACKWARD_TORQUE = 32768;
        sendBuf_byte[7] = (byte) ((BACKWARD_TORQUE + speed) / 256);
        sendBuf_byte[8] = (byte) ((BACKWARD_TORQUE + speed) % 256);
        sendBuf_byte[9] = 0;
        sendBuf_byte[10] = (byte) ((BACKWARD_TORQUE + speed) / 256);
        sendBuf_byte[11] = (byte) ((BACKWARD_TORQUE + speed) % 256);
        sendByte(sendBuf_byte);
    }

    public void stopWheel() {
        int speed = 0;
        sendBuf_byte[5] = 2;
        sendBuf_byte[6] = 0;
        sendBuf_byte[7] = (byte) (speed / 256);
        sendBuf_byte[8] = (byte) (speed % 256);
        sendBuf_byte[9] = 0;
        sendBuf_byte[10] = (byte) (speed / 256);
        sendBuf_byte[11] = (byte) (speed % 256);
        sendByte(sendBuf_byte);
    }

    public void setWheel(String[] params) {
        String param1 = params[2];

        switch (param1) {
            case "left":
                turnLeft();
                break;
            case "right":
                turnRight();
                break;
            case "straight":
                turnStraight();
                break;
        }
    }

    private void turnLeft() {
        sendBuf_byte[5] = 1;
        sendBuf_byte[24] = 0;
        sendByte(sendBuf_byte);
    }

    private void turnRight() {
        sendBuf_byte[5] = 3;
        sendBuf_byte[24] = 0;
        sendByte(sendBuf_byte);
    }

    private void turnStraight() {
        sendBuf_byte[5] = 2;
        sendBuf_byte[24] = 0;
        sendByte(sendBuf_byte);
    }

    public void setTurn(String[] params) {
        String param1 = params[2];
        String param2 = params[3];
        String param3 = params[4];
        int angle = 0;

        switch (param2) {
            case "left":
                if (param3.equals("1"))
                    angle = 10;
                if (param3.equals("2"))
                    angle = 20;
                if (param3.equals("3"))
                    angle = 30;
                if (param3.equals("4"))
                    angle = 50;

                sendBuf_byte[5] = (byte) (angle - 128);
                if (param1.equals("auto")) {
                    sendBuf_byte[24] = 2;
                } else if (param1.equals("step")) {
                    sendBuf_byte[24] = 1;
                }
                sendByte(sendBuf_byte);
                Log.d(TAG, Integer.toString(angle - 128));
                Log.d(TAG, Byte.toString(sendBuf_byte[5]));
                break;
            case "right":
                if (param3.equals("1"))
                    angle = 10;
                if (param3.equals("2"))
                    angle = 20;
                if (param3.equals("3"))
                    angle = 30;
                if (param3.equals("4"))
                    angle = 50;

                sendBuf_byte[5] = (byte) angle;
                if (param1.equals("auto")) {
                    sendBuf_byte[24] = 2;
                } else if (param1.equals("step")) {
                    sendBuf_byte[24] = 1;
                }
                sendByte(sendBuf_byte);
                break;
            case "straight":
                sendBuf_byte[5] = 0;
                sendBuf_byte[24] = 1;
                sendByte(sendBuf_byte);
                break;
        }

    }

    public void setMotor(String[] params) {
        String param = params[2];
        int speed = Integer.parseInt(params[3]);
        if (param.equals("on")) {
            sendBuf_byte[6] = 0;
            sendBuf_byte[7] = (byte) (speed / 256);
            sendBuf_byte[8] = (byte) (speed % 256);
            sendBuf_byte[9] = 0;
            sendBuf_byte[10] = (byte) (speed / 256);
            sendBuf_byte[11] = (byte) (speed % 256);
            sendByte(sendBuf_byte);
        } else {
            speed = 0;
            sendBuf_byte[6] = 0;
            sendBuf_byte[7] = (byte) (speed / 256);
            sendBuf_byte[8] = (byte) (speed % 256);
            sendBuf_byte[9] = 0;
            sendBuf_byte[10] = (byte) (speed / 256);
            sendBuf_byte[11] = (byte) (speed % 256);
            sendByte(sendBuf_byte);
        }
    }

    public void setbackMotor(String[] params) {
        String param = params[2];
        int speed = Integer.parseInt(params[3]);
        if (param.equals("on")) {
            sendBuf_byte[6] = 0;
            int BACKWARD_TORQUE = 32768;
            sendBuf_byte[7] = (byte) ((BACKWARD_TORQUE + speed) / 256);
            sendBuf_byte[8] = (byte) ((BACKWARD_TORQUE + speed) % 256);
            sendBuf_byte[9] = 0;
            sendBuf_byte[10] = (byte) ((BACKWARD_TORQUE + speed) / 256);
            sendBuf_byte[11] = (byte) ((BACKWARD_TORQUE + speed) % 256);
            sendByte(sendBuf_byte);
        } else {
            speed = 0;
            sendBuf_byte[6] = 0;
            sendBuf_byte[7] = (byte) (speed / 256);
            sendBuf_byte[8] = (byte) (speed % 256);
            sendBuf_byte[9] = 0;
            sendBuf_byte[10] = (byte) (speed / 256);
            sendBuf_byte[11] = (byte) (speed % 256);
            sendByte(sendBuf_byte);
        }
    }

    public void setDotMatrix(String[] params) {
        String param1 = params[2];
        String param2 = params[3];
        byte onoff = (byte) 0x00;
        if (param1.equals("on")) {
            onoff = (byte) 0xFF;
        }
        if (param2.equals("1")) {
            sendBuf_byte[13] = onoff;
        } else if (param2.equals("2")) {
            sendBuf_byte[14] = onoff;
        } else if (param2.equals("3")) {
            sendBuf_byte[15] = onoff;
        } else if (param2.equals("4")) {
            sendBuf_byte[16] = onoff;
        } else if (param2.equals("5")) {
            sendBuf_byte[17] = onoff;
        } else if (param2.equals("6")) {
            sendBuf_byte[18] = onoff;
        } else if (param2.equals("7")) {
            sendBuf_byte[19] = onoff;
        } else if (param2.equals("8")) {
            sendBuf_byte[20] = onoff;
        } else if (param2.equals("all")) {
            sendBuf_byte[13] = onoff;
            sendBuf_byte[14] = onoff;
            sendBuf_byte[15] = onoff;
            sendBuf_byte[16] = onoff;
            sendBuf_byte[17] = onoff;
            sendBuf_byte[18] = onoff;
            sendBuf_byte[19] = onoff;
            sendBuf_byte[20] = onoff;
        }
        sendBuf_byte[12] = 0;
        sendByte(sendBuf_byte);
    }

    public void setBuzzer(String[] params) {
        String param1 = params[2];
        String param2 = params[3];

        int buzzer = Integer.parseInt(param2);
        if (param1.equals("on")) {
            sendBuf_byte[22] = (byte) (buzzer);
            sendByte(sendBuf_byte);
        } else {
            sendBuf_byte[22] = 0;
            sendByte(sendBuf_byte);
        }
    }

    public void setAscii(String[] params) {
        String param1 = params[2];
        String param2 = params[3];

        if (param1.equals("off")) {
            sendBuf_byte[12] = 0;
            sendBuf_byte[13] = (byte) 0x00;
            sendBuf_byte[14] = (byte) 0x00;
            sendBuf_byte[15] = (byte) 0x00;
            sendBuf_byte[16] = (byte) 0x00;
            sendBuf_byte[17] = (byte) 0x00;
            sendBuf_byte[18] = (byte) 0x00;
            sendBuf_byte[19] = (byte) 0x00;
            sendBuf_byte[20] = (byte) 0x00;
        } else {
            if (param2.equals("A")) {
                sendBuf_byte[12] = (byte) 0X41; //dot led A
            } else if (param2.equals("B")) {
                sendBuf_byte[12] = (byte) 0X42;//dot led B
            } else if (param2.equals("C")) {
                sendBuf_byte[12] = (byte) 0X43;//dot led C
            } else if (param2.equals("D")) {
                sendBuf_byte[12] = (byte) 0X44;//dot led D
            } else if (param2.equals("E")) {
                sendBuf_byte[12] = (byte) 0X45;//dot led E
            } else if (param2.equals("F")) {
                sendBuf_byte[12] = (byte) 0X46;//dot led F
            } else if (param2.equals("G")) {
                sendBuf_byte[12] = (byte) 0X47;//dot led G
            } else if (param2.equals("H")) {
                sendBuf_byte[12] = (byte) 0X48;//dot led H
            } else if (param2.equals("I")) {
                sendBuf_byte[12] = (byte) 0X49;//dot led I
            } else if (param2.equals("J")) {
                sendBuf_byte[12] = (byte) 0X4A;//dot led J
            } else if (param2.equals("K")) {
                sendBuf_byte[12] = (byte) 0X4B;//dot led K
            } else if (param2.equals("L")) {
                sendBuf_byte[12] = (byte) 0X4C;//dot led L
            } else if (param2.equals("M")) {
                sendBuf_byte[12] = (byte) 0X4D;//dot led M
            } else if (param2.equals("N")) {
                sendBuf_byte[12] = (byte) 0X4E;//dot led N
            } else if (param2.equals("O")) {
                sendBuf_byte[12] = (byte) 0X4F;//dot led O
            } else if (param2.equals("P")) {
                sendBuf_byte[12] = (byte) 0X50;//dot led P
            } else if (param2.equals("Q")) {
                sendBuf_byte[12] = (byte) 0X51;//dot led Q
            } else if (param2.equals("R")) {
                sendBuf_byte[12] = (byte) 0X52;//dot led R
            } else if (param2.equals("S")) {
                sendBuf_byte[12] = (byte) 0X53;//dot led S
            } else if (param2.equals("T")) {
                sendBuf_byte[12] = (byte) 0X54;//dot led T
            } else if (param2.equals("U")) {
                sendBuf_byte[12] = (byte) 0X55;//dot led U
            } else if (param2.equals("V")) {
                sendBuf_byte[12] = (byte) 0X56;//dot led V
            } else if (param2.equals("W")) {
                sendBuf_byte[12] = (byte) 0X57;//dot led W
            } else if (param2.equals("X")) {
                sendBuf_byte[12] = (byte) 0X58;//dot led X
            } else if (param2.equals("Y")) {
                sendBuf_byte[12] = (byte) 0X59;//dot led Y
            } else if (param2.equals("Z")) {
                sendBuf_byte[12] = (byte) 0X5A;//dot led Z
            } else if (param2.equals("a")) {
                sendBuf_byte[12] = (byte) 0X61; //dot led a
            } else if (param2.equals("b")) {
                sendBuf_byte[12] = (byte) 0X62;//dot led b
            } else if (param2.equals("c")) {
                sendBuf_byte[12] = (byte) 0X63;//dot led c
            } else if (param2.equals("d")) {
                sendBuf_byte[12] = (byte) 0X64;//dot led d
            } else if (param2.equals("e")) {
                sendBuf_byte[12] = (byte) 0X65;//dot led e
            } else if (param2.equals("f")) {
                sendBuf_byte[12] = (byte) 0X66;//dot led f
            } else if (param2.equals("g")) {
                sendBuf_byte[12] = (byte) 0X67;//dot led g
            } else if (param2.equals("h")) {
                sendBuf_byte[12] = (byte) 0X68;//dot led h
            } else if (param2.equals("i")) {
                sendBuf_byte[12] = (byte) 0X69;//dot led i
            } else if (param2.equals("j")) {
                sendBuf_byte[12] = (byte) 0X6A;//dot led j
            } else if (param2.equals("k")) {
                sendBuf_byte[12] = (byte) 0X6B;//dot led k
            } else if (param2.equals("l")) {
                sendBuf_byte[12] = (byte) 0X6C;//dot led l
            } else if (param2.equals("m")) {
                sendBuf_byte[12] = (byte) 0X6D;//dot led m
            } else if (param2.equals("n")) {
                sendBuf_byte[12] = (byte) 0X6E;//dot led n
            } else if (param2.equals("o")) {
                sendBuf_byte[12] = (byte) 0X6F;//dot led o
            } else if (param2.equals("p")) {
                sendBuf_byte[12] = (byte) 0X70;//dot led p
            } else if (param2.equals("q")) {
                sendBuf_byte[12] = (byte) 0X71;//dot led q
            } else if (param2.equals("r")) {
                sendBuf_byte[12] = (byte) 0X72;//dot led r
            } else if (param2.equals("s")) {
                sendBuf_byte[12] = (byte) 0X73;//dot led s
            } else if (param2.equals("t")) {
                sendBuf_byte[12] = (byte) 0X74;//dot led t
            } else if (param2.equals("u")) {
                sendBuf_byte[12] = (byte) 0X75;//dot led u
            } else if (param2.equals("v")) {
                sendBuf_byte[12] = (byte) 0X76;//dot led v
            } else if (param2.equals("w")) {
                sendBuf_byte[12] = (byte) 0X77;//dot led w
            } else if (param2.equals("x")) {
                sendBuf_byte[12] = (byte) 0X78;//dot led x
            } else if (param2.equals("y")) {
                sendBuf_byte[12] = (byte) 0X79;//dot led y
            } else if (param2.equals("z")) {
                sendBuf_byte[12] = (byte) 0X7A;//dot led z
            } else if (param2.equals("0")) {
                sendBuf_byte[12] = (byte) 0X30; //dot led 0
            } else if (param2.equals("1")) {
                sendBuf_byte[12] = (byte) 0X31;//dot led 1
            } else if (param2.equals("2")) {
                sendBuf_byte[12] = (byte) 0X32;//dot led 2
            } else if (param2.equals("3")) {
                sendBuf_byte[12] = (byte) 0X33;//dot led 3
            } else if (param2.equals("4")) {
                sendBuf_byte[12] = (byte) 0X34;//dot led 4
            } else if (param2.equals("5")) {
                sendBuf_byte[12] = (byte) 0X35;//dot led 5
            } else if (param2.equals("6")) {
                sendBuf_byte[12] = (byte) 0X36;//dot led 6
            } else if (param2.equals("7")) {
                sendBuf_byte[12] = (byte) 0X37;//dot led 7
            } else if (param2.equals("8")) {
                sendBuf_byte[12] = (byte) 0X38;//dot led 8
            } else if (param2.equals("9")) {
                sendBuf_byte[12] = (byte) 0X39;//dot led 9
            } else if (param2.equals("!")) {
                sendBuf_byte[12] = (byte) 0x21;//dot led !
            }
        }
        sendByte(sendBuf_byte);
    }

    private void sendByte(byte[] Sendbuf) {
        tx_d[0] = 0x2;
        tx_d[1] = 28;

        tx_d[3] = 1;
        tx_d[4] = 1;
        tx_d[5] = Sendbuf[5]; // wheel steering
        tx_d[6] = Sendbuf[6]; // setup right motor mode
        tx_d[7] = Sendbuf[7]; // right motor high byte
        tx_d[8] = Sendbuf[8]; // right motor low byte
        tx_d[9] = Sendbuf[9]; // setup left motor mode
        tx_d[10] = Sendbuf[10];// left motor high byte
        tx_d[11] = Sendbuf[11];// left motor low byte
        tx_d[12] = Sendbuf[12];// ascii code
        tx_d[13] = Sendbuf[13];// Dot0
        tx_d[14] = Sendbuf[14];// Dot1
        tx_d[15] = Sendbuf[15];// Dot2
        tx_d[16] = Sendbuf[16];// Dot3
        tx_d[17] = Sendbuf[17];// Dot4
        tx_d[18] = Sendbuf[18];// Dot5
        tx_d[19] = Sendbuf[19];// Dot6
        tx_d[20] = Sendbuf[20];// Dot7
        tx_d[21] = 10;// IR Out Time Value
        tx_d[22] = Sendbuf[22];// buzzer
        tx_d[23] = Sendbuf[23];// LED Out
        tx_d[24] = Sendbuf[24];// SP
        tx_d[25] = 0;// SP
        tx_d[26] = 0;// SP
        tx_d[27] = 0x3;
        tx_d[2] = check_sum_tx_calcuration(27);
        try {
            if (sDriver != null)
                sDriver.write(tx_d, 0);
        } catch (IOException e) {
            e.printStackTrace();
            try {
                sDriver.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (Exception e2){
                e2.printStackTrace();
            }
            sDriver = null;
        }
    }

    private byte check_sum_tx_calcuration(int u16_cnt) {
        int u16_tx_check_sum = 0;
        int u16_tx_cnt;
        // u16_tx_check_sum = u8_tx_d[0];
        u16_tx_check_sum = u16_tx_check_sum + tx_d[1];
        for (u16_tx_cnt = 3; u16_tx_cnt <= u16_cnt; u16_tx_cnt++) {
            u16_tx_check_sum = u16_tx_check_sum + tx_d[u16_tx_cnt];
        }
        u16_tx_check_sum = u16_tx_check_sum % 256;
        return (byte) (u16_tx_check_sum);
    }

    public void recvRespMessage(StringBuffer recordValue) {
        sendByte(tx_d);
        int light = getData(25);
        if(light > -1 && light < 900){
            recordValue.append(String.format("sensor/light %d\n", light));
        }
        int temperature = getData(23);
        if(temperature > -1 && temperature < 900) {
            recordValue.append(String.format("sensor/temperature %d\n", temperature));
        }
        int torqueR = getData(19);
        if(torqueR > -1 && torqueR < 900) {
            recordValue.append(String.format("sensor/torqueR %d\n", torqueR));
        }
        int torqueL = getData(21);
        if(torqueL > -1 && torqueL < 900) {
            recordValue.append(String.format("sensor/torqueL %d\n", torqueL));
        }
        int ir1 = getData(7);
        if(ir1 > -1 && ir1 < 900) {
            recordValue.append(String.format("sensor/ir1 %d\n", ir1));
        }
        int ir2 = getData(9);
        if(ir2 > -1 && ir2 < 900) {
            recordValue.append(String.format("sensor/ir2 %d\n", ir2));
        }
        int ir3 = getData(11);
        if(ir3 > -1 && ir3 < 900) {
            recordValue.append(String.format("sensor/ir3 %d\n", ir3));
        }
        int ir4 = getData(13);
        if(ir4 > -1 && ir4 < 900) {
            recordValue.append(String.format("sensor/ir4 %d\n", ir4));
        }
        int ir5 = getData(15);
        if(ir5 > -1 && ir5 < 900) {
            recordValue.append(String.format("sensor/ir5 %d\n", ir5));
        }
        int ir6 = getData(17);
        if(ir6 > -1 && ir6 < 900) {
            recordValue.append(String.format("sensor/ir6 %d\n", ir6));
        }
    }
}