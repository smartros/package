package com.isans.smartros.android;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import org.ros.android.smartRosActivity;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class MainActivity extends smartRosActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private TextToSpeech tts = null;
    private HashMap<String, ApplicationInfo> mapPackage = new HashMap<>();
    private SpeechRecognizer sr;
    private int fps = 1;
    public static final int MARKER_ACTIVITY = 200;
    public static final int FACE_ACTIVITY = 300;
    public static final String MESSAGE = "MESSAGE";
    boolean bMarkered = false;
    boolean bFaced = false;
    public MainActivity() {
        super("Android Node");
    }
    BroadcastReceiver connectionUpdates = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int[] marker = intent.getIntArrayExtra("marker");
            if( marker != null ){
                Log.d(TAG, "marker id: " + marker[0]);
                StringBuffer recordValue = new StringBuffer();
                recordValue.append(String.format("recog_info/id/marker %d\n", marker[0]));
                recordValue.append(String.format("recog_info/x/marker %d\n", marker[1]));
                recordValue.append(String.format("recog_info/y/marker %d\n", marker[2]));
                recordValue.append(String.format("recog_info/size/marker %d\n", marker[3]));
                sendRespMessage(recordValue.toString());

                return ;
            }
            boolean notfound = intent.getBooleanExtra("not_found_marker", false);
            if( notfound ){
                StringBuffer recordValue = new StringBuffer();
                recordValue.append(String.format("recog_info/id/marker %d\n", 0));
                recordValue.append(String.format("recog_info/x/marker %d\n", 0));
                recordValue.append(String.format("recog_info/y/marker %d\n", 0));
                recordValue.append(String.format("recog_info/size/marker %d\n", 0));
                sendRespMessage(recordValue.toString());

                return ;
            }

            notfound = intent.getBooleanExtra("not_found_face", false);
            if( notfound ){
                StringBuffer recordValue = new StringBuffer();
                recordValue.append(String.format("recog_info/id/face %d\n", 0));
                recordValue.append(String.format("recog_info/x/face %d\n", 0));
                recordValue.append(String.format("recog_info/y/face %d\n", 0));
                recordValue.append(String.format("recog_info/size/face %d\n", 0));
                sendRespMessage(recordValue.toString());

                Log.d("tmp", " Faces detected : 0");
            }
            int[] face = intent.getIntArrayExtra("found_face");
            if(face != null ){
                Log.d("tmp", " Faces detected : 1");
                StringBuffer recordValue = new StringBuffer();
                recordValue.append(String.format("recog_info/id/face %d\n", face[0]));
                recordValue.append(String.format("recog_info/x/face %d\n", face[1]));
                recordValue.append(String.format("recog_info/y/face %d\n", face[2]));
                recordValue.append(String.format("recog_info/size/face %d\n", face[3]));
                sendRespMessage(recordValue.toString());
            }
        }
    };

    private void zeroMarker(){
        StringBuffer recordValue = new StringBuffer();
        recordValue.append(String.format("recog_info/id/marker %d\n", 0));
        recordValue.append(String.format("recog_info/x/marker %d\n", 0));
        recordValue.append(String.format("recog_info/y/marker %d\n", 0));
        recordValue.append(String.format("recog_info/size/marker %d\n", 0));
        sendRespMessage(recordValue.toString());
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sr = SpeechRecognizer.createSpeechRecognizer(this);
        sr.setRecognitionListener(new listener());
        startTTS();
        moveTaskToBack(true);   // turn background task
        //get a list of installed apps.
        final PackageManager pm = this.getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo packageInfo : packages) {
            String label = (String)packageInfo.loadLabel(pm);
            mapPackage.put(label, packageInfo);
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(
                connectionUpdates,
                new IntentFilter(MESSAGE));

        Intent intent = new Intent(this, WorkerService.class);
        startService(intent);
    }

    class listener implements RecognitionListener {
        @Override
        public void onReadyForSpeech(Bundle params) {

        }

        @Override
        public void onBeginningOfSpeech() {

        }

        @Override
        public void onRmsChanged(float rmsdB) {

        }

        @Override
        public void onBufferReceived(byte[] buffer) {

        }

        @Override
        public void onEndOfSpeech() {

        }

        @Override
        public void onError(int error) {
            Log.e(TAG, "error : " + error);
        }

        @Override
        public void onResults(Bundle results) {
            Log.d(TAG, "onResults " + results);
            ArrayList data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            StringBuilder recordValue = new StringBuilder();
            for (int i = 0; i < data.size(); i++) {
                Log.d(TAG, "result " + data.get(i));
            }
            recordValue.append(String.format("recog_info/word/speech %s\n", data.get(0)));
            sendRespMessage(recordValue.toString());
            Log.d(TAG, "results: " + String.valueOf(data.size()));
        }

        @Override
        public void onPartialResults(Bundle partialResults) {

        }

        @Override
        public void onEvent(int eventType, Bundle params) {

        }
    }
    protected void startTTS(){
        if( tts != null ) return ;
        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.KOREAN);
                }
            }
        });
    }
    protected void stopTTS(){
        if( tts == null ) return ;
        tts.stop();
        tts.shutdown();
        tts = null;
    }
    protected void sr_tts(String word){
        if (tts == null){
            Log.d(TAG, "not install tts engine");
            return ;
        }
        if( word.length() > 0 )
            tts.speak(word, TextToSpeech.QUEUE_FLUSH, null);
    }
    private final MyHandler mHandler = new MyHandler(this);

    private static class MyHandler extends Handler {
        private final WeakReference<MainActivity> mActivity;
        public MyHandler(MainActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity activity = mActivity.get();
            if (activity != null) {
                activity.handleMessage(msg);
            }
        }
    }
    protected void sr_recognition(String[] params){
        boolean start;
        switch( params[2] ) {
            case "start":
                start = true;
                break;
            case "stop":
                start = false;
                break;
            default:
                start = false;
                break;
        }
        if( params[3].equals("marker") ){
            if( start ){
                if( !bMarkered && !bFaced )
                    startMarker(params[4], params[5]);
            }
            else{
                if( bMarkered )
                    stopMarker();
            }
        }
        else if( params[3].equals("face") ){
            if( start ){
                if(!bFaced && !bMarkered){
                    startFace(params[4], params[5]);
                }
            }
            else{
                if( bFaced )
                    stopFace();
            }
        }
        else if( params[3].equals("speech") ){
            if( start ){
                Log.d(TAG, "start speech");
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE , "ko-KR");
                intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "voice.recognition.test");
                intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);
                sr.startListening(intent);
            }
            else {
                Log.d(TAG, "stop speech");
                // sr.stopListening(); may be auto deleted
                sendRespMessage("recog_info/word/speech  \n");
            }
        }
    }
    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        stopTTS();
        super.onDestroy();

        if( bMarkered )
            stopMarker();
        if( bFaced )
            stopFace();
    }

    @Override
    protected void onNewMessage(String s) {
        Message msg = new Message();
        msg.obj = s;
        mHandler.sendMessage(msg);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        moveTaskToBack(true);
        Log.d(TAG, "onActivityResult: " + resultCode);
    }

    private void handleMessage(Message msg) {
        String cmd = (String)msg.obj;
        String[] parts = cmd.split("/");
        Log.d(TAG, cmd);
        if (parts.length == 0) return;

        switch (parts[1]) {
            case "start_all":
                break;
            case "reset_all":
                break;
            case "fps":
                fps = Integer.parseInt(parts[2]);
                zeroMarker();   // send event by force
                break;
            case "sr_tts":
                if(parts.length > 2) {
                    sr_tts(parts[2]);
                    Log.d(TAG, "tts: " + parts[2]);
                }
                break;
            case "sr_recognition":
                if(parts.length > 2)
                    sr_recognition(parts);
                break;
            case "sr_take_picture":
                sr_take_picture();
                break;
            case "sr_start_package":
                if(parts.length > 2)
                    sr_start_package(parts[2]);
                break;
            case "sr_youtube":
                if(parts.length > 2)
                    sr_youtube(parts[2]);
                break;
            default:
                break;
        }
    }

    private void sr_youtube(String title) {
        String uri = "https://www.youtube.com/" + title;
        Intent open = new Intent(Intent.ACTION_VIEW, Uri.parse(uri.trim()));
        open.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        WorkerService.getInstance().startActivity(open);
    }

    private void sr_start_package(String name) {
        ApplicationInfo app = mapPackage.get(name);
        if (app != null) {
            final PackageManager pm = this.getPackageManager();
            Intent intent = pm.getLaunchIntentForPackage(app.packageName);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            WorkerService.getInstance().startActivity(intent);
        }
    }
    private void sr_take_picture() {
        Intent intent = new Intent();
        intent.setAction("android.media.action.IMAGE_CAPTURE");
        intent.addCategory("android.intent.category.DEFAULT");
        File file=new File("/mnt/sdcard/Download/image.jpg");
        Uri uri =Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        WorkerService.getInstance().startActivity(intent);
    }

    private void stopMarker(){

        finishActivity(MARKER_ACTIVITY);
        bMarkered = false;
    }
    private void startMarker(String camera, String display){
        bMarkered = true;
        final ActivityManager activity_manager = (ActivityManager) getSystemService (this.ACTIVITY_SERVICE);
        activity_manager.moveTaskToFront(getTaskId(), 0);
        Intent i = new Intent(MainActivity.this, MarkerBlockActivity.class);
        i.putExtra("camera", camera);
        i.putExtra("display", display);
        startActivityForResult(i, MARKER_ACTIVITY);
    }
    private void startFace(String camera, String display){
        Log.d("bcc", "startFAce");
        bFaced = true;
        final ActivityManager activity_manager = (ActivityManager) getSystemService (this.ACTIVITY_SERVICE);
        activity_manager.moveTaskToFront(getTaskId(), 0);
        Intent i = new Intent(MainActivity.this, FaceBlockActivity.class);
        i.putExtra("camera", camera);
        i.putExtra("display", display);
        startActivityForResult(i, FACE_ACTIVITY);
    }
    private void stopFace(){
        Log.d("bcc", "stopFace");
        finishActivity(FACE_ACTIVITY);
        bFaced = false;
    }
}
