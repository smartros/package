package com.isans.smartros.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.io.IOException;

public class FaceBlockActivity extends Activity implements SurfaceHolder.Callback {
	Camera camera;
	SurfaceView surfaceView;
	SurfaceHolder surfaceHolder;
	boolean previewing = false;

	TextView facedetect;

	DrawingView drawingView;
	Face[] detectedFaces;
	int position = 0;
	boolean display = false;
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupDefaultActivity();
		setContentView(R.layout.activity_main);

		getWindow().setFormat(PixelFormat.UNKNOWN);
		surfaceView = (SurfaceView) findViewById(R.id.surfaceview);
		surfaceHolder = surfaceView.getHolder();
		surfaceHolder.setFormat(PixelFormat.TRANSPARENT);
		surfaceHolder.addCallback(this);
		surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		drawingView = new DrawingView(this);
		LayoutParams layoutParamsDrawing = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		this.addContentView(drawingView, layoutParamsDrawing);

		facedetect = (TextView) findViewById(R.id.faces);

		Intent i = getIntent();
		String name = i.getStringExtra("camera");
		if(name.equals("front")){
			position = Camera.CameraInfo.CAMERA_FACING_FRONT;
		}
		else{
			position = Camera.CameraInfo.CAMERA_FACING_BACK;
		}
		name = i.getStringExtra("display");
		if(name.equals("on")){
			display = true;
		}
		else{
			display = false;
		}
	}
	protected void setupDefaultActivity() {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		this.getWindow().setFormat(PixelFormat.TRANSLUCENT);
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	}
	FaceDetectionListener faceDetectionListener = new FaceDetectionListener() {

		@Override
		public void onFaceDetection(Face[] faces, Camera camera) {

			if (faces.length == 0) {
				//facedetect.setText(" No Faces Detected! ");
				drawingView.setHaveFace(false);

				Intent intent = new Intent(MainActivity.MESSAGE);
				intent.putExtra("not_found_face", true);
				LocalBroadcastManager.getInstance(FaceBlockActivity.this).sendBroadcast(intent);

			} else {
				//facedetect.setText(String.valueOf(faces.length)
				//		+ " Faces detected");
				//Log.d("tmp", " Faces detected " + faces.length);
				drawingView.setHaveFace(true);
				detectedFaces = faces;
			}
			drawingView.invalidate();
		}
	};
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		if (previewing) {
			camera.stopFaceDetection();
			camera.stopPreview();
			previewing = false;
		}

		if (camera != null) {
			try {


				//Camera.Parameters parameters = camera.getParameters();
				//parameters.setPreviewSize(640, 480);
				//parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
				//parameters.setPictureFormat(ImageFormat.JPEG);
				//camera.setParameters(parameters);
				camera.setPreviewDisplay(surfaceHolder);
				//parameters.setPreviewSize(240,180);
				//camera.setParameters(parameters);
				camera.startPreview();
				camera.startFaceDetection();
				previewing = true;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		camera = Camera.open(position);
		//camera = Camera.open();
		try {
			camera.setPreviewTexture(new SurfaceTexture(10));
		} catch (IOException e1) {

		}
		camera.setFaceDetectionListener(faceDetectionListener);
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		camera.stopFaceDetection();
		camera.stopPreview();
		camera.release();
		camera = null;
		previewing = false;
	}

	private class DrawingView extends View {

		boolean haveFace;
		Paint drawingPaint;

		public DrawingView(Context context) {
			super(context);
			haveFace = false;
			drawingPaint = new Paint();
			drawingPaint.setColor(Color.YELLOW);
			drawingPaint.setStyle(Paint.Style.STROKE);
			drawingPaint.setStrokeWidth(3);
		}

		public void setHaveFace(boolean h) {
			haveFace = h;
		}

		@Override
		protected void onDraw(Canvas canvas) {
			if (haveFace) {

				int vWidth = getWidth();
				int vHeight = getHeight();

				for (int i = 0; i < detectedFaces.length; i++) {

					int l = detectedFaces[i].rect.left;
					int t = detectedFaces[i].rect.top;
					int r = detectedFaces[i].rect.right;
					int b = detectedFaces[i].rect.bottom;
					int left = (l + 1000) * vWidth / 2000;
					int top = (t + 1000) * vHeight / 2000;
					int right = (r + 1000) * vWidth / 2000;
					int bottom = (b + 1000) * vHeight / 2000;
					canvas.drawRect(left, top, right, bottom, drawingPaint);
					double averageLine = (left + top + right + bottom) / 4;
					int[] face = new int[4];
					face[0] = i+1;
					face[1] = left;
					face[2] = right;
					face[3] = (int) (averageLine * averageLine);
					Intent intent = new Intent(MainActivity.MESSAGE);
					intent.putExtra("found_face", face);
					LocalBroadcastManager.getInstance(FaceBlockActivity.this).sendBroadcast(intent);
				}
			} 
			else {
				canvas.drawColor(Color.TRANSPARENT);
			}
		}
	}
}
