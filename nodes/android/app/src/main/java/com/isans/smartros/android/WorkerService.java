package com.isans.smartros.android;


import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;

public class WorkerService extends Service {
    static public WorkerService theInstance = null;
    @Override
    public void onCreate() {
        theInstance = this;
    }

    static public WorkerService getInstance() {
        return theInstance;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}