package com.isans.smartros.android;

import android.content.Intent;
import android.content.res.AssetManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;

import java.io.IOException;

import javax.microedition.khronos.opengles.GL10;

import jp.androidgroup.nyartoolkit.markersystem.NyARAndMarkerSystem;
import jp.androidgroup.nyartoolkit.markersystem.NyARAndSensor;
import jp.androidgroup.nyartoolkit.sketch.AndSketch;
import jp.androidgroup.nyartoolkit.utils.camera.CameraPreview;
import jp.androidgroup.nyartoolkit.utils.gl.AndGLBox;
import jp.androidgroup.nyartoolkit.utils.gl.AndGLDebugDump;
import jp.androidgroup.nyartoolkit.utils.gl.AndGLFpsLabel;
import jp.androidgroup.nyartoolkit.utils.gl.AndGLTextLabel;
import jp.androidgroup.nyartoolkit.utils.gl.AndGLView;
import jp.nyatla.nyartoolkit.core.NyARException;
import jp.nyatla.nyartoolkit.core.types.NyARIntPoint2d;
import jp.nyatla.nyartoolkit.markersystem.NyARMarkerSystemConfig;

public class MarkerBlockActivity extends AndSketch implements AndGLView.IGLFunctionEvent
{
	CameraPreview _camera_preview;
	AndGLView _glv;
	Camera.Size _cap_size;
	int position = 0;
	boolean display = false;

	@Override
	public void onStart()
	{
		super.onStart();
		FrameLayout fr=((FrameLayout)this.findViewById(android.R.id.content));

		Intent i = getIntent();
		String name = i.getStringExtra("camera");
		if(name.equals("front")){
			position = Camera.CameraInfo.CAMERA_FACING_FRONT;
		}
		else{
			position = Camera.CameraInfo.CAMERA_FACING_BACK;
		}
		name = i.getStringExtra("display");
		if(name.equals("on")){
			display = true;
		}
		else{
			display = false;
		}

		this._camera_preview=new CameraPreview(this, position);
		this._cap_size=this._camera_preview.getRecommendPreviewSize(320,240);
		int h = this.getWindowManager().getDefaultDisplay().getHeight();
		int screen_w,screen_h;
		screen_w=(this._cap_size.width*h/this._cap_size.height);
		screen_h=h;
		//camera
		fr.addView(this._camera_preview, 0, new LayoutParams(screen_w,screen_h));
		//GLview
		this._glv=new AndGLView(this);
		fr.addView(this._glv, 0,new LayoutParams(screen_w,screen_h));

	}

	NyARAndSensor _ss;
	NyARAndMarkerSystem _ms;
	AndGLTextLabel text;
	AndGLBox box;
	AndGLFpsLabel fps;
	final int MARKER_MAX = 10;
	private int[] _mid = new int[MARKER_MAX];
	public void setupGL(GL10 gl)
	{
		try
		{
			AssetManager assetMng = getResources().getAssets();
			//create sensor controller.
			this._ss=new NyARAndSensor(this._camera_preview,this._cap_size.width,this._cap_size.height,30);
			//create marker system
			this._ms=new NyARAndMarkerSystem(new NyARMarkerSystemConfig(this._cap_size.width,this._cap_size.height));

			this._mid[0] = this._ms.addARMarker(assetMng.open("AR/data/marker_0.pat"), 16, 25, 80);
			this._mid[1] = this._ms.addARMarker(assetMng.open("AR/data/marker_1.pat"), 16, 25, 80);
			this._mid[2] = this._ms.addARMarker(assetMng.open("AR/data/marker_2.pat"), 16, 25, 80);
			this._mid[3] = this._ms.addARMarker(assetMng.open("AR/data/marker_3.pat"), 16, 25, 80);
			this._mid[4] = this._ms.addARMarker(assetMng.open("AR/data/marker_4.pat"), 16, 25, 80);
			this._mid[5] = this._ms.addARMarker(assetMng.open("AR/data/marker_5.pat"), 16, 25, 80);
			this._mid[6] = this._ms.addARMarker(assetMng.open("AR/data/marker_6.pat"), 16, 25, 80);
			this._mid[7] = this._ms.addARMarker(assetMng.open("AR/data/marker_7.pat"), 16, 25, 80);
			this._mid[8] = this._ms.addARMarker(assetMng.open("AR/data/marker_8.pat"), 16, 25, 80);
			this._mid[9] = this._ms.addARMarker(assetMng.open("AR/data/marker_9.pat"), 16, 25, 80);

			this._ss.start();
			//setup openGL Camera Frustum
			gl.glMatrixMode(GL10.GL_PROJECTION);
			gl.glLoadMatrixf(this._ms.getGlProjectionMatrix(),0);
			this.text=new AndGLTextLabel(this._glv);
			this.box=new AndGLBox(this._glv,40);
			this._debug=new AndGLDebugDump(this._glv);
			this.fps=new AndGLFpsLabel(this._glv,"MarkerPlaneActivity");
			this.fps.prefix=this._cap_size.width+"x"+this._cap_size.height+":";
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			this.finish();
		}
	}
	AndGLDebugDump _debug=null;

	public void drawGL(GL10 gl)
	{
		try{
			gl.glClearColor(0,0,0,0);
	        gl.glClear(GL10.GL_COLOR_BUFFER_BIT| GL10.GL_DEPTH_BUFFER_BIT);
	        if(ex!=null){
	        	_debug.draw(ex);
	        	return;
	        }
	        fps.draw(0, 0);
			synchronized(this._ss){
				this._ms.update(this._ss);
				boolean notfound = true;
				for (int i = 0; i < MARKER_MAX; i++) {
					if (_ms.isExistMarker(_mid[i])) {
						NyARIntPoint2d pts[] = _ms.getMarkerVertex2D(_mid[i]);
						double line1 = getLine(pts[0], pts[1]);
						double line2 = getLine(pts[1], pts[2]);
						double line3 = getLine(pts[2], pts[3]);
						double line4 = getLine(pts[3], pts[0]);

						double averageLine = (line1 + line2 + line3 + line4) / 4;
						int[] marker = new int[4];
						marker[0] = _mid[i];
						marker[1] = (pts[0].x + pts[1].x + pts[2].x + pts[3].x) / 4;
						marker[2] = (pts[0].y + pts[1].y + pts[2].y + pts[3].y) / 4;
						marker[3] = (int) (averageLine * averageLine);

						this.text.draw("found" + this._ms.getConfidence(this._mid[i]), 0, 16);
						notfound = false;

						Intent intent = new Intent(MainActivity.MESSAGE);
						intent.putExtra("marker", marker);
						LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

						gl.glMatrixMode(GL10.GL_MODELVIEW);
						gl.glLoadMatrixf(this._ms.getGlMarkerMatrix(this._mid[i]),0);
						this.box.draw(0,0,20);
					}
				}
				if( notfound ){
					Intent intent = new Intent(MainActivity.MESSAGE);
					intent.putExtra("not_found_marker", notfound);
					LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
				}
		}
		}catch(Exception e)
		{
			ex=e;
		}
	}
	Exception ex=null;
	private double getLine(NyARIntPoint2d pt1, NyARIntPoint2d pt2) {
		int dx = pt1.x - pt2.x;
		int dy = pt1.y - pt2.y;
		return Math.sqrt(dx * dx + dy * dy);
	}
}
