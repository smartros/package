package com.isans.smartros.mobilephone;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.Log;

import org.ros.android.smartRosActivity;

import java.util.List;


public class MainActivity extends smartRosActivity {
    int SHAKE_THRESHOLD=0;
    private static final String TAG = MainActivity.class.getSimpleName();
    int fps = 1;
    int count=0;
    // sensor
    private SensorManager sensorManager = null;
    List<Sensor> sensorList;
    // camera led
    private Camera camera = null;
    public MainActivity() {
        super("Mobile Phone Node");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorList = sensorManager.getSensorList(Sensor.TYPE_ALL);
        moveTaskToBack(true);   // turn background task
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy");
        if (camera != null) {
            camera.release();
        }
        unRegistSensors();
        super.onDestroy();
    }

    @Override
    protected void onNewMessage(String s) {
        Log.d(TAG, s);
        String[] parts = s.split("/");
        if (parts.length == 0) return;
        switch (parts[1]) {
            case "start_all":
                Log.d(TAG, "start_all");
                unRegistSensors();
                registeSensors();
                break;
            case "reset_all":
                Log.d(TAG, "reset_all");
                unRegistSensors();
                break;
            case "fps": // from system
                // used to delay time
                fps = Integer.parseInt(parts[2]);
                break;
            case "vibrator":
                Vibrator vbr = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vbr.vibrate((long) (Float.parseFloat(parts[2]) * 1000.0));
                break;
            case "camera-led":
                if (parts[2].equals("on")) {
                    if( camera == null )
                        camera = Camera.open();
                    final Camera.Parameters p = camera.getParameters();
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(p);
                    camera.startPreview();
                } else if (parts[2].equals("off")) {
                    if( camera == null ) break;
                    final Camera.Parameters p = camera.getParameters();
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    camera.setParameters(p);
                    camera.stopPreview();
                    camera.release();
                    camera = null;
                }
                break;
            case "shake_reset":
                count=0;
                break;
            case "shake_threshold":
                SHAKE_THRESHOLD=Integer.parseInt(parts[2]);
                break;
            default:
                break;
        }
    }

    public void registeSensors() {
        for (Sensor i : sensorList) {
            if (i.getType() == Sensor.TYPE_LIGHT ||
                    i.getType() == Sensor.TYPE_PROXIMITY ||
                    i.getType() == Sensor.TYPE_ACCELEROMETER ||
                    i.getType() == Sensor.TYPE_MAGNETIC_FIELD)
                sensorManager.registerListener(sensorListener, sensorManager.getDefaultSensor(i.getType()), SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    public void unRegistSensors() {
        sensorManager.unregisterListener(sensorListener);
    }

    private final SensorEventListener sensorListener = new SensorEventListener() {
        // fps calculate
        private long mStartTime = -1;
        private int mCounter;
        private int mFps;
        private int mDisplayCnt = 0;
        float[] m_acc_data = null, m_mag_data = null;
        float[] m_rotation = new float[9];
        float[] m_result_data = new float[3];
        private long lastTime;
        private float speed;
        private float lastX;
        private float lastY;
        private float lastZ;
        private float x, y, z;
        //private static final int SHAKE_THRESHOLD = 1000;

        int shake = 0;



        @Override
        public void onSensorChanged(SensorEvent event) {
            try {
                StringBuilder recordValue = new StringBuilder();

                if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
                    recordValue.append(String.format("light %f\n", event.values[0]));
                } else if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
                    recordValue.append(String.format("proximity %f\n", event.values[0]));
                } else if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                    recordValue.append(String.format("acceleration/x-axis %f\n", event.values[0]));
                    recordValue.append(String.format("acceleration/y-axis %f\n", event.values[1]));
                    recordValue.append(String.format("acceleration/z-axis %f\n", event.values[2]));

                    m_acc_data = event.values.clone();
                    long currentTime = System.currentTimeMillis();
                    long gabOfTime = (currentTime - lastTime);
                    if (gabOfTime > 100) {
                        lastTime=currentTime;
                        x = event.values[0];
                        y = event.values[1];
                        z = event.values[2];

                        speed = Math.abs(x + y + z - lastX - lastY - lastZ) / gabOfTime * 10000;

                        if (speed > SHAKE_THRESHOLD) {
                            shake = 1;
                            count++;
                        } else {
                            shake = 0;
                        }
                        lastX = event.values[0];
                        lastY = event.values[1];
                        lastZ = event.values[2];
                        recordValue.append(String.format("shake %d\n", shake));
                        recordValue.append(String.format("count %d\n", count));
                        Log.i(TAG, "shake: " + speed);
                    }
                } else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                    m_mag_data = event.values.clone();
                }
                if (m_acc_data != null && m_mag_data != null) {
                    // ref android sensor api
                    SensorManager.getRotationMatrix(m_rotation, null, m_acc_data, m_mag_data);
                    SensorManager.getOrientation(m_rotation, m_result_data);

                    // convert radian to degree
                    m_result_data[0] = (float) Math.toDegrees(m_result_data[0]);

                    // set range 0 to 360
                    if (m_result_data[0] < 0) m_result_data[0] += 360;
                    recordValue.append(String.format("orientation/azimuth %f\n", m_result_data[0]));
                    recordValue.append(String.format("orientation/pitch %f\n", Math.toDegrees(m_result_data[1])));
                    recordValue.append(String.format("orientation/roll %f\n", Math.toDegrees(m_result_data[2])));

                    m_acc_data = null;
                    m_mag_data = null;
                }
                // send to publisher data
                sendRespMessage(recordValue.toString());

                // fps calculate
                if (mStartTime == -1) {
                    mStartTime = SystemClock.elapsedRealtime();
                    mCounter = 0;
                }

                long now = SystemClock.elapsedRealtime();
                long delay = now - mStartTime;
                if (mDisplayCnt++ > mFps) {
                    mDisplayCnt = 0;
                    Log.i(TAG, "fps: " + mFps);
                }
                if (delay > 1000l) {
                    mStartTime = now;
                    mFps = mCounter;
                    mCounter = 0;
                }
                mCounter++;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };
}
